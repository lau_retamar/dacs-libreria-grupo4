﻿using LibreriaAPI.Modelo;


namespace LibreriaAPI.DAL.Interfaces
{
    
    public interface IRepositorioLineaInventario : IRepositorioGenerico<LineaInventario>
    {
    }
}
