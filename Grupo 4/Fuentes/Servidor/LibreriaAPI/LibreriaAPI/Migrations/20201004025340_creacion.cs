﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LibreriaAPI.Migrations
{
    public partial class creacion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Inventarios",
                columns: table => new
                {
                    InventarioId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fecha = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventarios", x => x.InventarioId);
                });

            migrationBuilder.CreateTable(
                name: "Libros",
                columns: table => new
                {
                    LibroId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ISBN = table.Column<int>(type: "int", nullable: false),
                    Titulo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Autor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fecha_Publicacion = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Fecha_Alta = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Baja = table.Column<bool>(type: "bit", nullable: false),
                    Editorial = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Idioma = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Libros", x => x.LibroId);
                });

            migrationBuilder.CreateTable(
                name: "Ubicacions",
                columns: table => new
                {
                    UbicacionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cantidad_Estantes = table.Column<int>(type: "int", nullable: false),
                    Libros_Maximos = table.Column<int>(type: "int", nullable: false),
                    Ocupada = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ubicacions", x => x.UbicacionId);
                });

            migrationBuilder.CreateTable(
                name: "LibroEnUbicacions",
                columns: table => new
                {
                    LibroEnUbicacionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Stock = table.Column<int>(type: "int", nullable: false),
                    LibrosLibroId = table.Column<int>(type: "int", nullable: true),
                    UbicacionId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibroEnUbicacions", x => x.LibroEnUbicacionId);
                    table.ForeignKey(
                        name: "FK_LibroEnUbicacions_Libros_LibrosLibroId",
                        column: x => x.LibrosLibroId,
                        principalTable: "Libros",
                        principalColumn: "LibroId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LibroEnUbicacions_Ubicacions_UbicacionId",
                        column: x => x.UbicacionId,
                        principalTable: "Ubicacions",
                        principalColumn: "UbicacionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LineaInventarios",
                columns: table => new
                {
                    LineaInventarioId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StockEnUbicacion = table.Column<int>(type: "int", nullable: false),
                    StockDelSistema = table.Column<int>(type: "int", nullable: false),
                    StockDiferencia = table.Column<int>(type: "int", nullable: false),
                    LibroEnUbicacionId = table.Column<int>(type: "int", nullable: true),
                    InventarioId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LineaInventarios", x => x.LineaInventarioId);
                    table.ForeignKey(
                        name: "FK_LineaInventarios_Inventarios_InventarioId",
                        column: x => x.InventarioId,
                        principalTable: "Inventarios",
                        principalColumn: "InventarioId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LineaInventarios_LibroEnUbicacions_LibroEnUbicacionId",
                        column: x => x.LibroEnUbicacionId,
                        principalTable: "LibroEnUbicacions",
                        principalColumn: "LibroEnUbicacionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LibroEnUbicacions_LibrosLibroId",
                table: "LibroEnUbicacions",
                column: "LibrosLibroId");

            migrationBuilder.CreateIndex(
                name: "IX_LibroEnUbicacions_UbicacionId",
                table: "LibroEnUbicacions",
                column: "UbicacionId");

            migrationBuilder.CreateIndex(
                name: "IX_LineaInventarios_InventarioId",
                table: "LineaInventarios",
                column: "InventarioId");

            migrationBuilder.CreateIndex(
                name: "IX_LineaInventarios_LibroEnUbicacionId",
                table: "LineaInventarios",
                column: "LibroEnUbicacionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LineaInventarios");

            migrationBuilder.DropTable(
                name: "Inventarios");

            migrationBuilder.DropTable(
                name: "LibroEnUbicacions");

            migrationBuilder.DropTable(
                name: "Libros");

            migrationBuilder.DropTable(
                name: "Ubicacions");
        }
    }
}
