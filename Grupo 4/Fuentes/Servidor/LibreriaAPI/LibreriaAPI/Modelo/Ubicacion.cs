﻿using System.Collections.Generic;


namespace LibreriaAPI.Modelo
{
    public class Ubicacion
    {
    
        public int UbicacionId { get; set; }
        public int Cantidad_Estantes { get; set; }
        public int Libros_Maximos { get; set; }
        public bool Ocupada { get; set; }

        public virtual IList<LibroEnUbicacion> LibroEnUbicacions { get; set; }

    }
}
