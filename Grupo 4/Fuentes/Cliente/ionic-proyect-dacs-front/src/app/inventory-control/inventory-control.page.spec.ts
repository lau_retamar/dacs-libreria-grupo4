import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InventoryControlPage } from './inventory-control.page';

describe('InventoryControlPage', () => {
  let component: InventoryControlPage;
  let fixture: ComponentFixture<InventoryControlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryControlPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InventoryControlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
