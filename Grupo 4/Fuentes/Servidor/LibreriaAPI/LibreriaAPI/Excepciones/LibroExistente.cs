﻿using System;


namespace LibreriaAPI.Excepciones
{
    public class LibroExistente : Exception
    {
        public LibroExistente(string pCadena) : base(pCadena)
        {

        }
    }
}
