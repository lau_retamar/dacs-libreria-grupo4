import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';


export interface Book {
  isbn: string,
  titulo: string,
  autor: string,
  fechaPublicacion: string,
  editorial: string,
  idioma: string,
  stock: number,
  ubicacionId: number
}

const httpOptions = {
  headers: new HttpHeaders({'Authorization': 'Bearer ' + localStorage.getItem("token")})
};

@Injectable({
  providedIn: 'root'
})

export class BookService {
  constructor(public http: HttpClient) {
    
  }

  addBook(jsonBook){
    return new Promise ((resolve, reject) => {
      this.http.post(environment.firebase.apiURL+'Libro/', jsonBook, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

  getBookByISBN(pISBN){
    return new Promise ((resolve, reject) => {
      this.http.get(environment.firebase.apiURL+'Libro/'+pISBN, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

  getBookByISBNFromGoogleBooks(pISBN){
    return new Promise ((resolve, reject) => {
      this.http.get(environment.firebase.apiURL+'APIGoogleBooks/'+pISBN, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

  getAllBooks(){
    return new Promise ((resolve, reject) => {
      console.log(httpOptions);
      this.http.get(environment.firebase.apiURL+'Libro', httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

  updateBook(pJSONBook){
    return new Promise((resolve, reject) => {
      this.http.put(environment.firebase.apiURL+'Libro/', pJSONBook, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

  updateStock(pISBN, pStock){
    return new Promise((resolve, reject) => {
      console.log(httpOptions);
      this.http.put(environment.firebase.apiURL+'Libro/ActualizarStock?pISBN='+pISBN+'&pStock='+pStock, httpOptions, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

  updateUbication(pISBN, pUbication){
    return new Promise((resolve, reject) =>{
      console.log(httpOptions);
      this.http.put(environment.firebase.apiURL+'Libro/ActualizarUbicaciónDeLibro?pISBN='+pISBN+'&pUbicacionId='+pUbication, httpOptions, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });  
  }

  deleteBook(pISBN){
    return new Promise((resolve, reject) =>{
      this.http.delete(environment.firebase.apiURL+'Libro/' + pISBN, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

}