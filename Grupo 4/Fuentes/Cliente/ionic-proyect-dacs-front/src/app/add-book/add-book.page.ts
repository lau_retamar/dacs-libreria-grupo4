import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertService } from '../services/alert-service/alert.service';
import { Book, BookService } from '../services/book-service/book.service';
import { LoadService } from '../services/load-service/load.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.page.html',
  styleUrls: ['./add-book.page.scss'],
})
export class AddBookPage implements OnInit {

  public book: Book;
  
  public idiomaActual: string;

  public isbnToSearch: number;
  public cantidad: number;
  public ubicacion: number;

  constructor(private bookService: BookService,
              private loadService: LoadService,
              private alertService: AlertService,
              private barcode: BarcodeScanner) { }

  ngOnInit() {
  }

  getBookToAdd(pIsbn) {
    this.loadService.presentLoading("Buscando libro...").then(presentLoading => {
      this.bookService.getBookByISBN(pIsbn).then((existingData: any) => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert('Error', 'Libro existente', 'El libro encontrado ya se encuentra registrado en la base de datos.');
      }).catch(
        error => {
          this.bookService.getBookByISBNFromGoogleBooks(pIsbn).then((data: any) => {
            this.loadService.loadingController.dismiss();
            this.book = data;
            switch (this.book.idioma) {
              case 'es':
                this.idiomaActual = 'Español'
                break;
              case 'en':
                this.idiomaActual = 'Inglés'
                break;
              default:
                break;
            }
          }).catch(
            error => {
              this.loadService.loadingController.dismiss();
              this.alertService.presentAlert("Error", "No se encontro ISBN ingresado", error.error);
            }
          )
        }
      )
    })
  }

  scan(){
    this.barcode.scan().then((barcodeData) => {
      this.getBookToAdd(barcodeData.text);
    }, (err) => {
      alert(JSON.stringify(err));
    })
  }

  addBook(){
    this.loadService.presentLoading("Agregando libro...").then(presentLoading => {
      let jsonBook = {
        "isbn": this.book.isbn,
        "titulo": this.book.titulo,
        "autor": this.book.autor,
        "fechaPublicacion": this.book.fechaPublicacion,
        "editorial": this.book.editorial,
        "idioma": this.book.idioma,
        "stock": this.cantidad,
        "ubicacionId": this.ubicacion
      }

      this.bookService.addBook(jsonBook).then((res) => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert('Éxito','Libro cargado','El libro se registró exitosamente.');
        this.book = null;
        this.isbnToSearch = null;
      }).catch((error) => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert("Error", "Error en la carga del libro", error.error);
      });
    });
  }

  cancel(){
    this.book = null;
  }

}


