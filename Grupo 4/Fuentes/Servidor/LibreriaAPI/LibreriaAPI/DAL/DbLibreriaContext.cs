﻿using LibreriaAPI.Modelo;
using Microsoft.EntityFrameworkCore;


namespace LibreriaAPI.DAL
{
    public class DbLibreriaContext:DbContext
    {
        public DbLibreriaContext()
        {
        }

        public DbLibreriaContext(DbContextOptions<DbLibreriaContext> options) : base(options)
        {
        }
       
        public DbSet<Inventario> Inventarios { get; set; }
        public DbSet<LineaInventario> LineaInventarios { get; set; }
        public DbSet<Libro> Libros { get; set; }
        public DbSet<Ubicacion> Ubicacions { get; set; }
        public DbSet<LibroEnUbicacion> LibroEnUbicacions { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
    }
}
