import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController } from '@ionic/angular';
import { AlertService } from '../services/alert-service/alert.service';
import { Book, BookService } from '../services/book-service/book.service';
import { LoadService } from '../services/load-service/load.service';

@Component({
  selector: 'app-update-stock',
  templateUrl: './update-stock.page.html',
  styleUrls: ['./update-stock.page.scss'],
})
export class UpdateStockPage implements OnInit {
  public isbnToSearch: number;
  private book: Book;
  public newStock: number;
  
  constructor(private bookService: BookService,
              private alertService: AlertService,
              public loadService: LoadService,
              private router: ActivatedRoute,
              private barcode: BarcodeScanner) { }

  ngOnInit() {
    let isbn = this.router.snapshot.paramMap.get('param');
    if (isbn != null) {
      this.loadService.presentLoading("Obteniendo libro...").then(presentLoading => {
        this.bookService.getBookByISBN(isbn).then((data: any) => {
          this.loadService.loadingController.dismiss();
          this.book = data;
        }).catch(
          error => {
            this.loadService.loadingController.dismiss();
            this.alertService.presentAlert("Error", "Error al obtener libro", error.error);
        });
      }) 
    }
  }

  scan(){
    this.barcode.scan().then((barcodeData) => {
      this.searchByISBN(barcodeData.text);
    }, (err) => {
      alert(JSON.stringify(err));
    })
  }

  searchByISBN(pISBN){
    this.book = null;
    this.loadService.presentLoading("Buscando libro...").then(presentLoading => {
      this.bookService.getBookByISBN(pISBN).then((data: any) => {
        this.loadService.loadingController.dismiss();
        this.book = data;
      }).catch(
        error => {
          this.loadService.loadingController.dismiss();
          this.alertService.presentAlert("Error", "No se encontro ISBN ingresado",error.error);
        }
      )
    })
  }

  updateStock(pISBN, pStock){
    this.loadService.presentLoading("Actualizando stock").then(presentLoading => {
      this.bookService.updateStock(pISBN, pStock).then((data: any) => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert("Aviso", "Stock modificado con éxito", "");
        this.book.stock = this.newStock;
        this.newStock = null;
      }).catch(
        error => {
            this.loadService.loadingController.dismiss();
            this.alertService.presentAlert("Error", "No se pudo actualizar stock", "");
        }
      )
    })
  }

}
