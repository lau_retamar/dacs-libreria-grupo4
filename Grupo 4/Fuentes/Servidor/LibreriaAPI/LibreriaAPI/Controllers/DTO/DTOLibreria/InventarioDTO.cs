﻿using System;


namespace LibreriaAPI.Controllers.DTO
{
    public class InventarioDTO
    {


        public int id { get; set; }
        public DateTime fecha { get; set; }
        public DateTime fechaInventario { get; set; }
        public string ISBN { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public DateTime fechaPublicacion { get; set; }
        public string editorial { get; set; }
        public string idioma { get; set; }
        public int ubicacion { get; set; }
        public int stockEnUbicacion { get; set; }
        public int stockDelSistema { get; set; }
        public int stockDiferencia { get; set; }
        public bool hideInformation { get; set; }







    }
}
