import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InventoryControlPage } from './inventory-control.page';

const routes: Routes = [
  {
    path: '',
    component: InventoryControlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InventoryControlPageRoutingModule {}
