﻿using LibreriaAPI.DAL.Interfaces;
using LibreriaAPI.DAL.Repositorio;
using System;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL
{
    public class UnidadDeTrabajo: IUnidadDeTrabajo
    {
        private readonly DbLibreriaContext iDbContext;
        
        public IRepositorioInventario RepositorioInventario { get; private set; }
        public IRepositorioLibro RepositorioLibro { get; private set; }
        public IRepositorioLibroEnUbicacion RepositorioLibroEnUbicacion { get; private set; }
        public IRepositorioLineaInventario RepositorioLineaInventario { get; private set; }
        public IRepositorioUbicacion RepositorioUbicacion { get; private set; }

        public UnidadDeTrabajo(DbLibreriaContext pContext)
        {
            if (pContext == null)
            {
                throw new ArgumentNullException(nameof(pContext));

            }
            iDbContext = pContext;
            RepositorioInventario = new RepositorioInventario(iDbContext);
            RepositorioLibro = new RepositorioLibro(iDbContext);
            RepositorioLibroEnUbicacion = new RepositorioLibroEnUbicacion(iDbContext);
            RepositorioLineaInventario = new RepositorioLineaInventario(iDbContext);
            RepositorioUbicacion = new RepositorioUbicacion(iDbContext);
        }
   

        /// <summary>
        /// Guarda los cambios la unidad de trabajo 
        /// </summary>
        public async Task Guardar()
        {
            await this.iDbContext.SaveChangesAsync();
        }

    }
}
