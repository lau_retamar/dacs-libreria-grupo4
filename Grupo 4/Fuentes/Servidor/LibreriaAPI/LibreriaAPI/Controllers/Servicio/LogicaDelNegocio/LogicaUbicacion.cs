﻿using AutoMapper;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.DAL;
using LibreriaAPI.Excepciones;
using LibreriaAPI.Modelo;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibreriaAPI.Controllers.Servicio.LogicaDelNegocio
{
    public class LogicaUbicacion
    {
        private readonly IUnidadDeTrabajo iUdt;
        private readonly IMapper iMapper;
        public LogicaUbicacion(IUnidadDeTrabajo pUdtcontext, IMapper pMapper)
        {
            iUdt = pUdtcontext;
            iMapper = pMapper;
        }
        public async Task<ActionResult<IEnumerable<UbicacionDTO>>> ObtenerUbicacionDTO()
        {
            IEnumerable<Ubicacion> listaUbicacion = await iUdt.RepositorioUbicacion.ObtenerTodos();
            if (listaUbicacion.Count()==0)
            {
                throw new UbicacionNoEncontrada("No se encontro listado de ubicaciones");
            }
            var listaUbicaconDTO = iMapper.Map<IList<UbicacionDTO>>(listaUbicacion);
            return listaUbicaconDTO.ToList();
        }
        public async Task<ActionResult<UbicacionDTO>> ObtenerUbicacionPorIdDTO(int id)
        {
        
            var mUbicacion = await iUdt.RepositorioUbicacion.Obtener(id);
            if (mUbicacion == null)
            {
                throw new UbicacionNoEncontrada("No se encontro Ubicación");
            }
            UbicacionDTO mUbicacionDTO = iMapper.Map<Ubicacion, UbicacionDTO>(mUbicacion);
            return mUbicacionDTO;
        }
        public async Task EliminarUbicacionDTO(int id)
        {
            var mUbicacionEliminar = await iUdt.RepositorioUbicacion.Obtener(id);
            if (mUbicacionEliminar == null)
            {
                throw new UbicacionNoEncontrada("No se encontro Ubicación");
            }
            iUdt.RepositorioUbicacion.Eliminar(mUbicacionEliminar);
            await iUdt.Guardar();
        }
        public async Task AgregarUbicacion(UbicacionDTO ubicacionDTO)
        {
            bool existencia =  iUdt.RepositorioUbicacion.ConsultarExistencia(ubicacionDTO.id);
            if (existencia == true)
            {
                throw new UbicacionExistente("Ya existe la Ubicación");
            }
            Ubicacion iUbicacion = new Ubicacion
            {
                Cantidad_Estantes = ubicacionDTO.cantidadEstantes,
                Libros_Maximos = ubicacionDTO.librosMaximos,
                Ocupada = ubicacionDTO.ocupado
            };
            await iUdt.RepositorioUbicacion.Agregar(iUbicacion);
            await iUdt.Guardar();
        }
        public async Task ModificarUbicacion(UbicacionDTO ubicacionDTO)
        {
            bool existencia = iUdt.RepositorioUbicacion.ConsultarExistencia(ubicacionDTO.id);
            if (existencia == false)
            {
                throw new UbicacionNoEncontrada("No se encontro Ubicación");
            }
            var mUbicacion = await iUdt.RepositorioUbicacion.Obtener(ubicacionDTO.id);
            mUbicacion.Cantidad_Estantes = ubicacionDTO.cantidadEstantes;
            mUbicacion.Libros_Maximos = ubicacionDTO.librosMaximos;
            mUbicacion.Ocupada = ubicacionDTO.ocupado;
            iUdt.RepositorioUbicacion.ModificarEntidades(mUbicacion);
            await iUdt.Guardar();
        }
    }
}
