﻿using LibreriaAPI.DAL.Interfaces;
using LibreriaAPI.Modelo;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL.Repositorio
{
    public class RepositorioLibroEnUbicacion : RepositorioGenerico<LibroEnUbicacion, DbLibreriaContext>, IRepositorioLibroEnUbicacion
    {
       
        public RepositorioLibroEnUbicacion(DbLibreriaContext pContext) : base(pContext)
        {

        }
        public async Task<LibroEnUbicacion> ObtengoLibroUbicacionISBN(string pISBN)
        {
            return await iDbContext.LibroEnUbicacions.Where(x=>x.Libros.ISBN==pISBN).FirstOrDefaultAsync();
        }
        public bool ConsultarExistencia(string pisbn)
        {
            return iDbContext.LibroEnUbicacions.Any(x => x.Libros.ISBN == pisbn);
        }
        public async Task<IList<LibroEnUbicacion>> ObtengoListaTodosLosLibroUbicacionId(int pIdUbicacion)
        {
            return await iDbContext.LibroEnUbicacions.Where(x => x.Ubicacion.UbicacionId == pIdUbicacion).ToListAsync();
        }
    }
}
