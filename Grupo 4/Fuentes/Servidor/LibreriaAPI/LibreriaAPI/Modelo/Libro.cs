﻿using System;
using System.Collections.Generic;


namespace LibreriaAPI.Modelo
{
    public class Libro
    {
        
        public int LibroId { get; set; }
        public string ISBN { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public DateTime Fecha_Publicacion { get; set; }
        public DateTime Fecha_Alta { get; set; }
        public bool Baja { get; set; }
        public string Editorial { get; set; }
        public string Idioma { get; set; }
        
    
        public virtual IList<LibroEnUbicacion> LibroEnUbicacions { get; set; }
    }
}
