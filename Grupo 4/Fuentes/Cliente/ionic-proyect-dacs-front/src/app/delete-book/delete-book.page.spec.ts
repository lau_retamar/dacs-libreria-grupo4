import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeleteBookPage } from './delete-book.page';

describe('DeleteBookPage', () => {
  let component: DeleteBookPage;
  let fixture: ComponentFixture<DeleteBookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteBookPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeleteBookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
