import { Component, OnInit } from '@angular/core';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import { MenuController } from '@ionic/angular';
import { AlertService } from '../services/alert-service/alert.service';
import { AuthService } from '../services/auth-service/auth.service';
import { Book, BookService } from '../services/book-service/book.service';
import { LoadService } from '../services/load-service/load.service';

@Component({
  selector: 'app-search-book',
  templateUrl: './search-book.page.html',
  styleUrls: ['./search-book.page.scss'],
})
export class SearchBookPage implements OnInit {
  public isbnToSearch: number;
  private book: Book;

  constructor(private barcode: BarcodeScanner,
              private bookService: BookService,
              private alertService: AlertService,
              public loadService: LoadService,
              private menuCtrl: MenuController) {
                
    this.menuCtrl.enable(true);            
  }

  ngOnInit() {
  }

  scan(){
    this.barcode.scan().then((barcodeData) => {
      this.searchByISBN(barcodeData.text);
    }, (err) => {
      alert(JSON.stringify(err));
    })
  }

  searchByISBN(pISBN){
    this.loadService.presentLoading("Buscando libro...").then(presentLoading => {
      this.bookService.getBookByISBN(pISBN).then((data: any) => {
        this.loadService.loadingController.dismiss();
        this.book = data;
      }).catch(error => {
        this.bookService.getBookByISBNFromGoogleBooks(pISBN).then((dataOfGoogle: any) => {
          this.loadService.loadingController.dismiss();
          this.book = dataOfGoogle;
        }).catch(error =>{
          this.loadService.loadingController.dismiss();
          this.alertService.presentAlert("Error", "No se encontro ISBN ingresado",error.error);
        })
      })  
    })
  }

}
