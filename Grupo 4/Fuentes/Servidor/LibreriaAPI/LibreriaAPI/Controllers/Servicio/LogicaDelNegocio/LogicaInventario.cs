﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.DAL;
using LibreriaAPI.Excepciones;
using LibreriaAPI.Modelo;
using Microsoft.AspNetCore.Mvc;



namespace LibreriaAPI.Controllers.Servicio.LogicaDelNegocio
{
    public class LogicaInventario
    {
        private readonly IUnidadDeTrabajo iUdt;
       
        public LogicaInventario(IUnidadDeTrabajo pUdtcontext)
        {
            iUdt = pUdtcontext;
        }

        public async Task<ActionResult<IEnumerable<InventarioDTO>>> ObtenerInventarioDTO()
        {
            IEnumerable<Inventario> listaInventario = await iUdt.RepositorioInventario.ObtenerTodos();
            if (listaInventario.Count() == 0)
            {
                throw new InventarioNoEncontrado("No se encontro listado de inventarios");
            }
            IList<InventarioDTO> listaInventarioDTO = new List<InventarioDTO>();
            IList<InventarioDTO> listaVariosInventario = new List<InventarioDTO>();
            foreach (Inventario item in listaInventario)
            {
                listaInventarioDTO = MappeoManual(item, listaVariosInventario);
            }
            return listaInventarioDTO.ToList();
        }
        public async Task<ActionResult<IList<InventarioDTO>>> ObtenerInventarioPorIdDTO(int id)
        {
            var mInventario = await iUdt.RepositorioInventario.Obtener(id);
            if (mInventario == null)
            {
                throw new InventarioNoEncontrado("No se encontro Inventario");
            }
            IList<InventarioDTO> listaUnInventario = new List<InventarioDTO>();
            IList<InventarioDTO> mInventarioDTO = MappeoManual(mInventario, listaUnInventario);
            return mInventarioDTO.ToList();
        }

        private IList<InventarioDTO> MappeoManual(Inventario mInventario, IList<InventarioDTO> listaInventario)
        {
            foreach (var item in mInventario.LineaInventarios)
            {
                InventarioDTO iIventarioDTO = new InventarioDTO()
                {
                    id=item.Inventario.InventarioId,
                    fechaInventario=item.Inventario.Fecha,
                    autor = item.LibroEnUbicacion.Libros.Autor,
                    editorial = item.LibroEnUbicacion.Libros.Editorial,
                    fecha = item.LibroEnUbicacion.Libros.Fecha_Alta,
                    fechaPublicacion = item.LibroEnUbicacion.Libros.Fecha_Publicacion,
                    idioma = item.LibroEnUbicacion.Libros.Idioma,
                    ISBN = item.LibroEnUbicacion.Libros.ISBN,
                    stockDelSistema = item.StockDelSistema,
                    stockDiferencia = item.StockDiferencia,
                    stockEnUbicacion = item.StockEnUbicacion,
                    ubicacion = item.LibroEnUbicacion.Ubicacion.UbicacionId,
                    titulo = item.LibroEnUbicacion.Libros.Titulo,
                };
                listaInventario.Add(iIventarioDTO);
            }
            
            return listaInventario;
        }
        public async Task EliminarInventarioDTO(int id)
        {
            var mInventarioEliminar = await iUdt.RepositorioInventario.Obtener(id);
            if (mInventarioEliminar == null)
            {
                throw new InventarioNoEncontrado("No se encontro Inventario");
            }
            iUdt.RepositorioInventario.Eliminar(mInventarioEliminar);
            await iUdt.Guardar();
        }

        public async Task AgregarInventario(IList<InventarioDTO> ListInventarioDTO)
        {
            Inventario iInventario = new Inventario();
            IList<LineaInventario> iListaLineaInventario = new List<LineaInventario>();
            foreach (InventarioDTO itemDTO in ListInventarioDTO)
            {
                await ConsultarExistenciaUbicacion(itemDTO);
                await RegistrarInventario(itemDTO, iInventario, iListaLineaInventario);
            }
            await iUdt.RepositorioInventario.Agregar(iInventario);
            await iUdt.Guardar();
        }
        private async Task ConsultarExistenciaUbicacion(InventarioDTO pInventarioDTO)
        {
            bool existencia = iUdt.RepositorioInventario.ConsultarExistencia(pInventarioDTO.id);
            if (existencia == true)
            {
                throw new InventarioExistente("Ya existe el inventario");
            }
            Ubicacion iUbicacion = await iUdt.RepositorioUbicacion.Obtener(pInventarioDTO.ubicacion);
            if (iUbicacion == null)
            {
                throw new UbicacionNoEncontrada("No se encontro ubicación");
            }
        }
        private async Task RegistrarInventario(InventarioDTO inventarioDTO, Inventario iInventario, IList<LineaInventario> iListaLineaInventario)
        {
                int stockDiferencia = 0;
                LibroEnUbicacion iLibroEnUbicacion = await iUdt.RepositorioLibroEnUbicacion.ObtengoLibroUbicacionISBN(Convert.ToString(inventarioDTO.ISBN));
                if (iLibroEnUbicacion==null)
                {
                    throw new LibroNoEncontrado("No se encontro el ISBN del Libro");
                }
                stockDiferencia =  CalcularStockDiferencia(inventarioDTO, iLibroEnUbicacion.Stock);
                LineaInventario iLineaInventario = new LineaInventario
                    {
                        StockDelSistema = iLibroEnUbicacion.Stock,
                        StockDiferencia = stockDiferencia,
                        StockEnUbicacion = inventarioDTO.stockEnUbicacion,
                        LibroEnUbicacion = await iUdt.RepositorioLibroEnUbicacion.Obtener(iLibroEnUbicacion.LibroEnUbicacionId)
                    };
                iListaLineaInventario.Add(iLineaInventario);
                iLibroEnUbicacion.Stock = inventarioDTO.stockEnUbicacion;
                iInventario.Fecha = inventarioDTO.fecha;
                iInventario.LineaInventarios = iListaLineaInventario;
        }

        private  int  CalcularStockDiferencia(InventarioDTO inventarioDTO, int pStock)
        {
            int diferencia = 0;
            if (pStock > inventarioDTO.stockEnUbicacion)
            {
                  diferencia = (-1) * (pStock - inventarioDTO.stockEnUbicacion);
            }
            else
            {
                diferencia = inventarioDTO.stockEnUbicacion - pStock;
            }
            return diferencia;
        }
        public async Task ModificarInventario(InventarioDTO InventarioDTO)
        {
            bool existencia = iUdt.RepositorioInventario.ConsultarExistencia(InventarioDTO.id);
            if (existencia == false)
            {
                throw new InventarioNoEncontrado("No se encontro Inventario");
            }
            bool existenciaISBN=  iUdt.RepositorioLibroEnUbicacion.ConsultarExistencia(Convert.ToString(InventarioDTO.ISBN));
            if (existenciaISBN == false)
            {
                throw new LibroNoEncontrado("No se encontro el ISBN del Libro");
            }
            var mInventario = await iUdt.RepositorioInventario.Obtener(InventarioDTO.id);
            mInventario.Fecha = InventarioDTO.fecha;
            foreach (LineaInventario item in mInventario.LineaInventarios)
            {
                if (item.LibroEnUbicacion.Libros.ISBN==InventarioDTO.ISBN)
                {
                    item.StockEnUbicacion = InventarioDTO.stockEnUbicacion;
                    item.LibroEnUbicacion.Stock = InventarioDTO.stockEnUbicacion;
                    item.StockDiferencia = CalcularStockDiferencia(InventarioDTO, item.StockDelSistema);
                    item.StockDelSistema = item.StockDelSistema;
                }
              
            }
            iUdt.RepositorioInventario.ModificarEntidades(mInventario);
            await iUdt.Guardar();
        }
        public async Task<ActionResult<IEnumerable<InventarioDTO>>> TodosLosLibrosEnUnaUbicacion(int id)
        {
            IEnumerable<LibroEnUbicacion> listaLibroUbicacion = await iUdt.RepositorioInventario.ObtengoListaTodosLosLibrosPorUbicacion(id);
            if (listaLibroUbicacion.Count() == 0)
            {
                throw new LibroNoEncontrado("No se encontro listado de libros en esa ubicación");
            }
            IList<InventarioDTO> listaVariosDTO = new List<InventarioDTO>();
            return MappeoManualLibrosDeUnaUbicacion(listaLibroUbicacion, listaVariosDTO).ToList();
        }

        private IList<InventarioDTO> MappeoManualLibrosDeUnaUbicacion(IEnumerable<LibroEnUbicacion> pLibroUbicacion, IList<InventarioDTO> listaVariosDTO)
        {
            foreach (var item in pLibroUbicacion)
            {
                InventarioDTO iIventarioDTO = new InventarioDTO()
                {

                    stockDiferencia = 0,
                    stockEnUbicacion = 0,
                    stockDelSistema = item.Stock,
                    autor = item.Libros.Autor,
                    editorial = item.Libros.Editorial,
                    fecha = item.Libros.Fecha_Alta,
                    fechaPublicacion = item.Libros.Fecha_Publicacion,
                    idioma = item.Libros.Idioma,
                    ISBN = item.Libros.ISBN,
                    ubicacion = item.Ubicacion.UbicacionId,
                    titulo = item.Libros.Titulo,
                    hideInformation = false,
                };
                listaVariosDTO.Add(iIventarioDTO);
            }
            return listaVariosDTO;
        }
    }
}
