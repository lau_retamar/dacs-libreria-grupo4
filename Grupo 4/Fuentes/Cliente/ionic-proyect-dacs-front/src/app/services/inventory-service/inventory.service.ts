import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { promise } from 'protractor';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Authorization': 'Bearer ' + localStorage.getItem("token")})
};

@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  httpOptions: any;

  constructor(public http: HttpClient) {}

  getBooksOfObication(pUbication: number){
    return new Promise ((resolve, reject) => {
      this.http.get(environment.firebase.apiURL+'Inventario/GetLibros/'+pUbication, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }

  addInventory(jsonInventory){
    return new Promise((resolve, reject) => {
      this.http.post(environment.firebase.apiURL+'Inventario/', jsonInventory, httpOptions).subscribe(
        data => resolve(data),
        error => reject(error)
      )
    });
  }
}
