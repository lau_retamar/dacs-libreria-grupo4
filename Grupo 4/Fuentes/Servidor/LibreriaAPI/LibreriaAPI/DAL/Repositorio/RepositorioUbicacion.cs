﻿using LibreriaAPI.DAL.Interfaces;
using LibreriaAPI.Modelo;
using System.Linq;


namespace LibreriaAPI.DAL.Repositorio
{
    public class RepositorioUbicacion : RepositorioGenerico<Ubicacion, DbLibreriaContext>, IRepositorioUbicacion
    {
       
        public RepositorioUbicacion(DbLibreriaContext pContext) : base(pContext)
        {

        }
        public bool ConsultarExistencia(int pid)
        {
           return iDbContext.Ubicacions.Any(x => x.UbicacionId == pid);
        }
    }
}
