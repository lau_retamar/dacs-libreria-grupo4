import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InventoryControlPageRoutingModule } from './inventory-control-routing.module';

import { InventoryControlPage } from './inventory-control.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InventoryControlPageRoutingModule
  ],
  declarations: [InventoryControlPage]
})
export class InventoryControlPageModule {}
