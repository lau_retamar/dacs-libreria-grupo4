﻿using LibreriaAPI.DAL.Interfaces;
using LibreriaAPI.Modelo;


namespace LibreriaAPI.DAL.Repositorio
{
    public class RepositorioLibro : RepositorioGenerico<Libro, DbLibreriaContext>, IRepositorioLibro
    {
      
        public RepositorioLibro(DbLibreriaContext pContext) : base(pContext)
        {

        }
    }
}
