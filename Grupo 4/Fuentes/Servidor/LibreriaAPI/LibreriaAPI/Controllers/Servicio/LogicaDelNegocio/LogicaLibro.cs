﻿using AutoMapper;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.Controllers.Servicio.APIs_externas;
using LibreriaAPI.DAL;
using LibreriaAPI.Excepciones;
using LibreriaAPI.Modelo;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibreriaAPI.Controllers.Servicio.LogicaDelNegocio
{
    public class LogicaLibro
    {
        private readonly IUnidadDeTrabajo iUdt;
        private readonly IMapper iMapper;
        public LogicaLibro(IUnidadDeTrabajo pUdtcontext, IMapper pMapper)
        {
            iUdt = pUdtcontext;
            iMapper = pMapper;
        }
        public async Task<ActionResult<IEnumerable<LibroDTO>>> ObtenerLibrosEnUbicacionDTO()
        {
            IEnumerable<LibroEnUbicacion> listaLibro = await iUdt.RepositorioLibroEnUbicacion.ObtenerTodos();
            if (listaLibro.Count() == 0)
            {
                throw new LibroNoEncontrado("No se encontro listado de libros en Ubicacion");
            }
            var listaLibroUbicaconDTO = iMapper.Map<IList<LibroDTO>>(listaLibro);
            return listaLibroUbicaconDTO.ToList();
        }
        public async Task<ActionResult<LibroDTO>> ObtenerLibroEnUbicacionPorISBNDTO(string pISBN)
        {
            var mLibro = await iUdt.RepositorioLibroEnUbicacion.ObtengoLibroUbicacionISBN(pISBN);
            if (mLibro == null)
            {
                throw new LibroNoEncontrado("No se encontro Libro");
            }
            LibroDTO mLibroUbicacionDTO = iMapper.Map<LibroEnUbicacion, LibroDTO>(mLibro);
            return mLibroUbicacionDTO;
        }
        public async Task EliminarLibroDTO(string pISBN)
        {
            var mLibroEliminar = await iUdt.RepositorioLibroEnUbicacion.ObtengoLibroUbicacionISBN(pISBN);
            if (mLibroEliminar == null)
            {
                throw new LibroNoEncontrado("No se encontro Libro");
            }
            iUdt.RepositorioLibroEnUbicacion.Eliminar(mLibroEliminar);
            iUdt.RepositorioLibro.Eliminar(mLibroEliminar.Libros);
            await iUdt.Guardar();
        }

        public async Task AgregarLibro(LibroDTO pLibroDTO)
        {
            
            var mLibroEnBD = await iUdt.RepositorioLibroEnUbicacion.ObtengoLibroUbicacionISBN(pLibroDTO.ISBN);
            if (mLibroEnBD == null)
            {
               
                Ubicacion iUbicacion =await iUdt.RepositorioUbicacion.Obtener(pLibroDTO.ubicacionId);
                if (iUbicacion==null)
                {
                    throw new UbicacionNoEncontrada("No se encontro ubicacion");
                }
                LibroEnUbicacion iLibroEnUbicacion = new LibroEnUbicacion
                {
                    Stock = pLibroDTO.stock,
                    Ubicacion = iUbicacion,
                    Libros= new Libro
                    {
                        ISBN= pLibroDTO.ISBN,
                        Autor= pLibroDTO.autor,
                        Baja=false,
                        Editorial = pLibroDTO.editorial,
                        Fecha_Alta =DateTime.Now,
                        Fecha_Publicacion = pLibroDTO.fechaPublicacion,
                        Idioma = pLibroDTO.idioma,
                        Titulo = pLibroDTO.titulo,
                     }
                };
                await iUdt.RepositorioLibroEnUbicacion.Agregar(iLibroEnUbicacion);
                await iUdt.Guardar();
            }
            else
            {
                throw new LibroExistente("El libro ya se encuentra registrado");
            }
        }
        private async Task<ActionResult<LibroDTO>> ObtengoLibroDeAPIGoogleBooks(string pISBN)
        {
            string idioma = "";
            var googlestring = await APIGoogleBooks.ConsultarISBNAsync(pISBN);
            ApiGoogleLibroDTO mApiGoogleDTO = JsonConvert.DeserializeObject<ApiGoogleLibroDTO>(googlestring);
            if (mApiGoogleDTO.totalItems != "0")
            {
                if (mApiGoogleDTO.items[0].volumeInfo.language == "es")
                {
                    idioma = "Español";
                }
                else
                {
                    idioma = "Ingles";
                }
                LibroDTO mLibro = new LibroDTO()
                {
                    ISBN = pISBN,
                    titulo = mApiGoogleDTO.items[0].volumeInfo.title,
                    autor = Convert.ToString(mApiGoogleDTO.items[0].volumeInfo.authors.ElementAt(0)),
                    fechaPublicacion = Convert.ToDateTime(mApiGoogleDTO.items[0].volumeInfo.publishedDate),
                    editorial = mApiGoogleDTO.items[0].volumeInfo.publisher,
                    idioma = idioma,
                };
                return mLibro;
            }
            else
            {
                throw new LibroEnApiGoogleNoEncontrado("Google no encontro libro");
            }

        }
        public async Task ModificarLibro(LibroDTO pLibroDTO)
        {
            bool existencia = iUdt.RepositorioLibroEnUbicacion.ConsultarExistencia(pLibroDTO.ISBN);
            if (existencia == false)
            {
                throw new LibroNoEncontrado("No se encontro Libro");
            }
            bool existenciaUbicacion = iUdt.RepositorioUbicacion.ConsultarExistencia(pLibroDTO.ubicacionId);
            if (existenciaUbicacion == false)
            {
                throw new UbicacionNoEncontrada("No se encontro Ubicacion");
            }
            var mLibro = await iUdt.RepositorioLibroEnUbicacion.ObtengoLibroUbicacionISBN(pLibroDTO.ISBN);
            mLibro.Stock = pLibroDTO.stock;
            mLibro.Libros.Autor = pLibroDTO.autor;
            mLibro.Libros.Baja = false;
            mLibro.Libros.Editorial = pLibroDTO.editorial;
            mLibro.Libros.Fecha_Alta = DateTime.Now;
            mLibro.Libros.Fecha_Publicacion = pLibroDTO.fechaPublicacion;
            mLibro.Libros.Idioma = pLibroDTO.idioma;
            mLibro.Libros.ISBN = pLibroDTO.ISBN;
            mLibro.Libros.Titulo = pLibroDTO.titulo;
            mLibro.Ubicacion.Ocupada = true;
            mLibro.Ubicacion = await iUdt.RepositorioUbicacion.Obtener(pLibroDTO.ubicacionId);
            iUdt.RepositorioLibroEnUbicacion.ModificarEntidades(mLibro);
            await iUdt.Guardar();
        }
        public async Task<ActionResult<IEnumerable<LibroDTO>>> ObtenerLibrosSinUbicacionDTO()
        {
            IEnumerable<LibroEnUbicacion> listaLibro = await iUdt.RepositorioLibroEnUbicacion.ObtenerTodos();
            if (listaLibro.Count() == 0)
            {
                throw new LibroNoEncontrado("No se encontro listado de libros");
            }
             var listaLibroUbicaconDTO = iMapper.Map<IList<LibroDTO>>(listaLibro).Where(x=>x.ubicacionId==1);
            return listaLibroUbicaconDTO.ToList();
        }
        public async Task ModificarLibroStock(string pISBN, int pStock)
        {
            bool existencia = iUdt.RepositorioLibroEnUbicacion.ConsultarExistencia(pISBN);
            if (existencia == false)
            {
                throw new LibroNoEncontrado("No se encontro Libro");
            }
            if (pStock<0)
            {
                throw new StockNoValido("El stock no puede ser negativo");
            }
            var mLibro = await iUdt.RepositorioLibroEnUbicacion.ObtengoLibroUbicacionISBN(pISBN);
            mLibro.Stock = pStock;
            iUdt.RepositorioLibroEnUbicacion.ModificarEntidades(mLibro);
            await iUdt.Guardar();
        }
        public async Task ModificarLibroUbicacion(string pISBN, int pUbicacion)
        {
            bool existencia = iUdt.RepositorioLibroEnUbicacion.ConsultarExistencia(pISBN);
            if (existencia == false)
            {
                throw new LibroNoEncontrado("No se encontro Libro");
            }
            bool existenciaUbicacioon = iUdt.RepositorioUbicacion.ConsultarExistencia(pUbicacion);
            if (existenciaUbicacioon == false)
            {
                throw new UbicacionNoEncontrada("No se encontro Ubicación");
            }
            var mLibro = await iUdt.RepositorioLibroEnUbicacion.ObtengoLibroUbicacionISBN(pISBN);
            mLibro.Ubicacion =await iUdt.RepositorioUbicacion.Obtener(pUbicacion);
            iUdt.RepositorioLibroEnUbicacion.ModificarEntidades(mLibro);
            await iUdt.Guardar();
        }
    }
}
