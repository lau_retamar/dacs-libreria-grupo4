import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetUbicationPageRoutingModule } from './set-ubication-routing.module';

import { SetUbicationPage } from './set-ubication.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetUbicationPageRoutingModule
  ],
  declarations: [SetUbicationPage]
})
export class SetUbicationPageModule {}
