﻿using AutoMapper;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.Modelo;

namespace LibreriaAPI.Controllers.Servicio.Mapper
{
    public class VistaADominio : Profile
    {
        public VistaADominio()
        {
            CreateMap<Inventario, InventarioDTO>()
          .ForMember(dto => dto.autor, map => map.MapFrom(u => u.LineaInventarios[1].LibroEnUbicacion.Libros.Autor))
          .ForMember(dto => dto.fecha, map => map.MapFrom(u => u.Fecha)).ReverseMap();

        }
    }
}
