﻿using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Threading.Tasks;


namespace LibreriaAPI.Controllers.Servicio.APIs_externas
{
    public class APIGoogleBooks
    {/// <summary>
    /// Consumo servicio googlebooks api
    /// </summary>
    /// <param name="pISBN">recibo un ISBN</param>
    /// <returns></returns>
        public static async Task<string> ConsultarISBNAsync(string pISBN)
        {
            var builder = new ConfigurationBuilder()
               .AddJsonFile("UrlGoogle.json", optional: false, reloadOnChange: true);
            var configurationUrlGoogle = builder.Build();
            string link = configurationUrlGoogle["GoogleApi:urlBook"] + pISBN;
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(link);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
           
        }

    }
}
