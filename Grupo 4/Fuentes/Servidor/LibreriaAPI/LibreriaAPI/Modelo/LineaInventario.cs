﻿namespace LibreriaAPI.Modelo
{
    public class LineaInventario
    {

       
        public int LineaInventarioId { get; set; }
        public int StockEnUbicacion { get; set; }
        public int StockDelSistema { get; set; }
        public int StockDiferencia { get; set; }
      
        public virtual LibroEnUbicacion LibroEnUbicacion { get; set; }
        

        public virtual Inventario Inventario { get; set; }
    }
}
