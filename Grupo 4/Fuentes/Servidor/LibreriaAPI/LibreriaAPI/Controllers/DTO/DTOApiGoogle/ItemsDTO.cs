﻿namespace LibreriaAPI.Controllers.DTO
{
    public class ItemsDTO
    {
        public string id { get; set; }
        public string etag { get; set; }
        public VolumeInfoDTO volumeInfo { get; set; }
    }
}
