﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LibreriaAPI.Controllers.Servicio.APIs_externas;
using LibreriaAPI.Controllers.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace LibreriaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class APIGoogleBooksController : ControllerBase
    {
        private LogicaAPIGoogle iLogicaAPIGoogle;
        public APIGoogleBooksController(LogicaAPIGoogle pLogicaApiGoogle)
        {
            iLogicaAPIGoogle = pLogicaApiGoogle;
        }
        /// <summary>
        /// Consume servicio ApiGoogle
        /// </summary>
        /// <param name="pISBN">se ingresa un isbn</param>
        /// <returns>un LibroDTO</returns>
        [HttpGet("{pISBN}")]
        public async Task<ActionResult<LibroDTO>> GetAPIGoogleBooks(string pISBN)
        {
                return await iLogicaAPIGoogle.ObtenerLibroGooglePorISBN(pISBN);
        }
    }
}
