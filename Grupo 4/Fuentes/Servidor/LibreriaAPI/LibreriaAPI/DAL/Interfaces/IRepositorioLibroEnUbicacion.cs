﻿using LibreriaAPI.Modelo;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL.Interfaces
{
   
    public interface IRepositorioLibroEnUbicacion : IRepositorioGenerico<LibroEnUbicacion>
    {
        Task<LibroEnUbicacion> ObtengoLibroUbicacionISBN(string pISBN);
        bool ConsultarExistencia(string pisbn);
        Task<IList<LibroEnUbicacion>> ObtengoListaTodosLosLibroUbicacionId(int pIdUbicacion);
    }
}
