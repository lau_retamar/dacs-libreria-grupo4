﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.Controllers.Servicio.LogicaDelNegocio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace LibreriaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class InventarioController : ControllerBase
    {
         
        private LogicaInventario iLogicaInventario;
        public InventarioController(LogicaInventario pLogicaInventario)
        {
            iLogicaInventario = pLogicaInventario;
        }
        /// <summary>
        /// Listado de todos los inventarios
        /// </summary>
        /// <returns>Devuelve un listado de todos los inventarios</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InventarioDTO>>> GetInventarioDTO()
        {        
              return await iLogicaInventario.ObtenerInventarioDTO();
        }
        /// <summary>
        /// Obtiene un inventario
        /// </summary>
        /// <param name="id">busca por id de inventario</param>
        /// <returns>Devuelve un inventario</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IList<InventarioDTO>>> GetInventarioDTO(int id)
        {
                return await iLogicaInventario.ObtenerInventarioPorIdDTO(id);
        }
        /// <summary>
        /// Modificar un inventario
        /// </summary>
        /// <param name="InventarioDTO"></param>
        /// <returns>Inventario Modificado</returns>
        [HttpPut]
        public async Task<IActionResult> PutInventarioDTO(InventarioDTO InventarioDTO)
        {
                await iLogicaInventario.ModificarInventario(InventarioDTO);
                return Ok("Se modificó Inventario");
        }
        /// <summary>
        /// Agregar inventario
        /// </summary>
        /// <param name="pListaInventarioDTO">recibe una lista inventario</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> PostInventarioDTO(IList<InventarioDTO> pListaInventarioDTO)
        {
                await iLogicaInventario.AgregarInventario(pListaInventarioDTO);
                return Ok("Se registró Inventario");
        }
        /// <summary>
        /// Elimina un inventario de la base de datos
        /// </summary>
        /// <param name="id">Id del inventario a eliminar</param>
        /// <returns>Inventario eliminado</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteInventarioDTO(int id)
        {
                await iLogicaInventario.EliminarInventarioDTO(id);
                return Ok("Se elimino Inventario");
        }
        /// <summary>
        /// Obtengo todos los libros en la ubicacion
        /// </summary>
        /// <param name="pIdUbicacion"></param>
        /// <returns></returns>
        [HttpGet("GetLibros/{pIdUbicacion}")]
        public async Task<ActionResult<IEnumerable<InventarioDTO>>> GetTodosLosLibrosEnUbicacion(int pIdUbicacion)
        { 
                return await iLogicaInventario.TodosLosLibrosEnUnaUbicacion(pIdUbicacion);
        }

    }
}
