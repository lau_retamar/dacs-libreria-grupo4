﻿using AutoMapper;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.Modelo;


namespace LibreriaAPI.Controllers.Servicios.Mapper
{
    public class DominioAVista: Profile
    {
        public DominioAVista()
        {
           
            CreateMap<Inventario, InventarioDTO>()
                  .ForMember(dto => dto.id, map => map.MapFrom(u => u.InventarioId))
                  .ForMember(dto => dto.autor, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Libros.Autor))
                  .ForMember(dto => dto.editorial, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Libros.Editorial))
                  .ForMember(dto => dto.fecha, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Libros.Fecha_Alta))
                  .ForMember(dto => dto.fechaPublicacion, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Libros.Fecha_Publicacion))
                  .ForMember(dto => dto.idioma, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Libros.Idioma))
                  .ForMember(dto => dto.ISBN, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Libros.ISBN))
                  .ForMember(dto => dto.stockDelSistema, map => map.MapFrom(u => u.LineaInventarios[0].StockDelSistema))
                  .ForMember(dto => dto.stockDiferencia, map => map.MapFrom(u => u.LineaInventarios[0].StockDiferencia))
                  .ForMember(dto => dto.stockEnUbicacion, map => map.MapFrom(u => u.LineaInventarios[0].StockEnUbicacion))
                  .ForMember(dto => dto.titulo, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Libros.Titulo))
                  .ForMember(dto => dto.ubicacion, map => map.MapFrom(u => u.LineaInventarios[0].LibroEnUbicacion.Ubicacion.UbicacionId)) ;

            CreateMap<Ubicacion, UbicacionDTO>()
                  .ForMember(dto => dto.id, map => map.MapFrom(u => u.UbicacionId))
                  .ForMember(dto => dto.cantidadEstantes, map => map.MapFrom(u => u.Cantidad_Estantes))
                  .ForMember(dto => dto.librosMaximos, map => map.MapFrom(u => u.Libros_Maximos))
                  .ForMember(dto => dto.ocupado, map => map.MapFrom(u => u.Ocupada));

            CreateMap<LibroEnUbicacion, LibroDTO>()
             .ForMember(dto => dto.autor, map => map.MapFrom(u => u.Libros.Autor))
             .ForMember(dto => dto.editorial, map => map.MapFrom(u => u.Libros.Editorial))
             .ForMember(dto => dto.fechaPublicacion, map => map.MapFrom(u => u.Libros.Fecha_Publicacion))
             .ForMember(dto => dto.idioma, map => map.MapFrom(u => u.Libros.Idioma))
             .ForMember(dto => dto.ISBN, map => map.MapFrom(u => u.Libros.ISBN))
             .ForMember(dto => dto.stock, map => map.MapFrom(u => u.Stock))
             .ForMember(dto => dto.titulo, map => map.MapFrom(u => u.Libros.Titulo))
             .ForMember(dto => dto.ubicacionId, map => map.MapFrom(u => u.Ubicacion.UbicacionId));
        }
    }
}
