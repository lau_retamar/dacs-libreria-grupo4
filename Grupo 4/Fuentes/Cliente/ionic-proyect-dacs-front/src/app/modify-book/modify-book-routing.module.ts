import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModifyBookPage } from './modify-book.page';

const routes: Routes = [
  {
    path: '',
    component: ModifyBookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModifyBookPageRoutingModule {}
