USE [LibreriaDB]
GO
/****** Object:  User [IIS APPPOOL\libreriaApi]    Script Date: 26/10/2020 12:15:18 ******/
CREATE USER [IIS APPPOOL\libreriaApi] FOR LOGIN [IIS APPPOOL\libreriaApi] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [libreria]    Script Date: 26/10/2020 12:15:18 ******/
CREATE USER [libreria] FOR LOGIN [libreria] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [NT AUTHORITY\IUSR]    Script Date: 26/10/2020 12:15:18 ******/
CREATE USER [NT AUTHORITY\IUSR] FOR LOGIN [NT AUTHORITY\IUSR]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\libreriaApi]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [IIS APPPOOL\libreriaApi]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [IIS APPPOOL\libreriaApi]
GO
ALTER ROLE [db_datareader] ADD MEMBER [IIS APPPOOL\libreriaApi]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [IIS APPPOOL\libreriaApi]
GO
ALTER ROLE [db_owner] ADD MEMBER [libreria]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [libreria]
GO
ALTER ROLE [db_datareader] ADD MEMBER [libreria]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [libreria]
GO
ALTER ROLE [db_owner] ADD MEMBER [NT AUTHORITY\IUSR]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [NT AUTHORITY\IUSR]
GO
ALTER ROLE [db_datareader] ADD MEMBER [NT AUTHORITY\IUSR]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [NT AUTHORITY\IUSR]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 26/10/2020 12:15:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventarios]    Script Date: 26/10/2020 12:15:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventarios](
	[InventarioId] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Inventarios] PRIMARY KEY CLUSTERED 
(
	[InventarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LibroEnUbicacions]    Script Date: 26/10/2020 12:15:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibroEnUbicacions](
	[LibroEnUbicacionId] [int] IDENTITY(1,1) NOT NULL,
	[Stock] [int] NOT NULL,
	[LibrosLibroId] [int] NULL,
	[UbicacionId] [int] NULL,
 CONSTRAINT [PK_LibroEnUbicacions] PRIMARY KEY CLUSTERED 
(
	[LibroEnUbicacionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Libros]    Script Date: 26/10/2020 12:15:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Libros](
	[LibroId] [int] IDENTITY(1,1) NOT NULL,
	[ISBN] [nvarchar](max) NULL,
	[Titulo] [nvarchar](max) NULL,
	[Autor] [nvarchar](max) NULL,
	[Fecha_Publicacion] [datetime2](7) NOT NULL,
	[Fecha_Alta] [datetime2](7) NOT NULL,
	[Baja] [bit] NOT NULL,
	[Editorial] [nvarchar](max) NULL,
	[Idioma] [nvarchar](max) NULL,
 CONSTRAINT [PK_Libros] PRIMARY KEY CLUSTERED 
(
	[LibroId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LineaInventarios]    Script Date: 26/10/2020 12:15:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LineaInventarios](
	[LineaInventarioId] [int] IDENTITY(1,1) NOT NULL,
	[StockEnUbicacion] [int] NOT NULL,
	[StockDelSistema] [int] NOT NULL,
	[StockDiferencia] [int] NOT NULL,
	[LibroEnUbicacionId] [int] NULL,
	[InventarioId] [int] NULL,
 CONSTRAINT [PK_LineaInventarios] PRIMARY KEY CLUSTERED 
(
	[LineaInventarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ubicacions]    Script Date: 26/10/2020 12:15:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ubicacions](
	[UbicacionId] [int] IDENTITY(1,1) NOT NULL,
	[Cantidad_Estantes] [int] NOT NULL,
	[Libros_Maximos] [int] NOT NULL,
	[Ocupada] [bit] NOT NULL,
 CONSTRAINT [PK_Ubicacions] PRIMARY KEY CLUSTERED 
(
	[UbicacionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201004025340_creacion', N'5.0.0-rc.1.20451.13')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201010145901_s', N'5.0.0-rc.1.20451.13')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201012213338_ge', N'5.0.0-rc.1.20451.13')
GO
SET IDENTITY_INSERT [dbo].[Inventarios] ON 
GO
INSERT [dbo].[Inventarios] ([InventarioId], [Fecha]) VALUES (5, CAST(N'2020-10-13T20:21:10.4110000' AS DateTime2))
GO
INSERT [dbo].[Inventarios] ([InventarioId], [Fecha]) VALUES (6, CAST(N'2020-10-12T22:28:22.0340000' AS DateTime2))
GO
INSERT [dbo].[Inventarios] ([InventarioId], [Fecha]) VALUES (7, CAST(N'2020-10-12T18:57:54.6810986' AS DateTime2))
GO
INSERT [dbo].[Inventarios] ([InventarioId], [Fecha]) VALUES (8, CAST(N'2020-10-19T02:05:17.7510000' AS DateTime2))
GO
INSERT [dbo].[Inventarios] ([InventarioId], [Fecha]) VALUES (9, CAST(N'2020-10-13T20:23:17.5120000' AS DateTime2))
GO
INSERT [dbo].[Inventarios] ([InventarioId], [Fecha]) VALUES (10, CAST(N'2020-10-13T20:23:17.5120000' AS DateTime2))
GO
INSERT [dbo].[Inventarios] ([InventarioId], [Fecha]) VALUES (11, CAST(N'2020-10-13T20:23:17.5120000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[Inventarios] OFF
GO
SET IDENTITY_INSERT [dbo].[LibroEnUbicacions] ON 
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (1, 20, 1, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (2, 44, 2, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (3, 3, 5, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (4, 18, 6, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (5, 55, 7, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (6, 4, 8, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (7, 2, 9, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (8, 222, 10, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (9, 90, 11, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (10, 23, 12, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (11, 2, 13, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (12, 988, 14, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (13, 2, 15, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (14, 25, 16, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (15, 2, 17, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (16, 555, 18, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (17, 0, 19, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (18, 2, 20, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (19, 2, 21, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (20, 36, 22, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (21, 50, 23, 2)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (22, 20, 24, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (23, 25, 25, 1)
GO
INSERT [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId], [Stock], [LibrosLibroId], [UbicacionId]) VALUES (24, 30, 26, 1)
GO
SET IDENTITY_INSERT [dbo].[LibroEnUbicacions] OFF
GO
SET IDENTITY_INSERT [dbo].[Libros] ON 
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (1, N'1684128870', N'Harry Potter Crochet', N'feef', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-16T15:10:32.5092410' AS DateTime2), 0, N'ss', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (2, N'1408856778', N'Harry Potter: The Complete Collection', N'gg', CAST(N'2014-10-09T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-16T10:11:30.8456512' AS DateTime2), 0, N'jjj', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (5, N'0439358078', N'Harry Potter and the Order of the Phoenix (Book 5)', NULL, CAST(N'2015-01-10T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-12T21:58:56.1958086' AS DateTime2), 0, N'ss', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (6, N'1338340565', N'Quidditch Through the Ages: The Illustrated Edition', NULL, CAST(N'2020-10-06T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-12T22:01:08.1320059' AS DateTime2), 0, N'Scholastic Incorporated', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (7, N'0545790352', N'Harry Potter and the Sorcerer''s Stone', N'J. K. Rowling', CAST(N'2015-10-06T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-12T22:25:32.8169934' AS DateTime2), 0, N'Arthur A. Levine Books', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (8, N'1948174448', N'd', N'd', CAST(N'2019-09-19T16:15:54.7900000' AS DateTime2), CAST(N'2020-10-19T16:18:13.2010334' AS DateTime2), 0, N'd', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (9, N'1338322966', N'Harry Potter Origami: Fifteen Paper-Folding Projects Straight from the Wizarding World! (Harry Potter)', N'Scholastic', CAST(N'2019-06-25T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-18T23:48:49.4977108' AS DateTime2), 0, N'Scholastic Incorporated', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (10, N'1683838262', N'Harry Potter: Knitting Magic', N'Tanis Gray', CAST(N'2020-01-28T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-18T23:50:30.2479670' AS DateTime2), 0, N'Insight Editions', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (11, N'1338030019', N'Harry Potter Magical Places & Characters Coloring Book', N'Inc. Scholastic', CAST(N'2016-03-29T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-18T23:54:12.5296522' AS DateTime2), 0, N'Scholastic Incorporated', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (12, N'1948174243', N'The Unofficial Ultimate Harry Potter Spellbook', N'Media Lab Books', CAST(N'2019-06-18T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-19T00:16:39.9462962' AS DateTime2), 0, N'Media Lab Books', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (13, N'1683834070', N'Harry Potter: A Pop-Up Guide to Hogwarts', N'Matthew Reinhart', CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-20T21:24:15.9205599' AS DateTime2), 0, N'Insight Editions', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (14, N'1694106683', N'How to Draw Harry Potter Step by Step Drawings!', N'Drawing Land', CAST(N'2019-09-18T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-19T00:26:24.0411974' AS DateTime2), 0, N'jjj', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (15, N'1683834585', N'Harry Potter: Hogwarts School of Witchcraft and Wizardry (Tiny Book)', N'Jody Revenson', CAST(N'2020-03-17T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-19T00:34:02.0622395' AS DateTime2), 0, N'Insight Editions', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (16, N'1683839404', N'Harry Potter: Christmas at Hogwarts', N'Jody Revenson', CAST(N'2020-09-15T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-20T21:20:26.3469920' AS DateTime2), 0, N'Insight Kids', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (17, N'8498386950', N'Harry Potter y La Camara Secreta', N'J. K. Rowling', CAST(N'2015-06-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-20T21:16:10.0642069' AS DateTime2), 0, N'Salamandra', N'en')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (18, N'8409212110', N'Europa frente a EE.UU. y China. Prevenir el declive en la era de la inteligencia artificial', N'Andrés Pedreño', CAST(N'2020-06-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-20T12:17:22.0643684' AS DateTime2), 0, N'jjjj', N'es')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (19, N'2', N'string', N'string', CAST(N'2020-10-24T23:38:02.0900000' AS DateTime2), CAST(N'2020-10-24T20:40:40.6526239' AS DateTime2), 0, N'string', N'string')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (20, N'430', N'', N'', CAST(N'2016-01-17T07:44:29.0000000' AS DateTime2), CAST(N'2020-10-20T17:07:16.1566825' AS DateTime2), 0, N'string', N'string')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (21, N'65', N'', N'', CAST(N'2016-01-17T07:44:29.0000000' AS DateTime2), CAST(N'2020-10-20T17:09:52.3259009' AS DateTime2), 0, N'string', N'string')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (22, N'3', N'', N'', CAST(N'2016-01-17T07:44:29.0000000' AS DateTime2), CAST(N'2020-10-20T17:21:53.9364216' AS DateTime2), 0, N'string', N'string')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (23, N'242424', N'', N'', CAST(N'2016-01-17T07:44:29.0000000' AS DateTime2), CAST(N'2020-10-21T10:00:20.9881594' AS DateTime2), 0, N'string', N'string')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (24, N'34', N'', N'', CAST(N'2016-01-17T07:44:29.0000000' AS DateTime2), CAST(N'2020-10-23T12:35:53.6457181' AS DateTime2), 0, N'string', N'string')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (25, N'77', N'', N'', CAST(N'2016-01-17T07:44:29.0000000' AS DateTime2), CAST(N'2020-10-23T12:52:57.3411237' AS DateTime2), 0, N'string', N'string')
GO
INSERT [dbo].[Libros] ([LibroId], [ISBN], [Titulo], [Autor], [Fecha_Publicacion], [Fecha_Alta], [Baja], [Editorial], [Idioma]) VALUES (26, N'2888', N'', N'', CAST(N'2016-01-17T07:44:29.0000000' AS DateTime2), CAST(N'2020-10-23T14:41:01.5960772' AS DateTime2), 0, N'string', N'string')
GO
SET IDENTITY_INSERT [dbo].[Libros] OFF
GO
SET IDENTITY_INSERT [dbo].[LineaInventarios] ON 
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (4, 66, 90, -24, 1, NULL)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (5, 18, 20, -2, 1, 5)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (6, 44, 20, 24, 2, 6)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (7, 33, 10, 23, 1, 6)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (8, 31, 33, -2, 1, 7)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (9, 19, 18, 1, 1, 8)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (10, 15, 19, -4, 1, 9)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (11, 20, 15, 5, 1, 10)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (12, 20, 23, -3, 4, 10)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (13, 20, 20, 0, 1, 11)
GO
INSERT [dbo].[LineaInventarios] ([LineaInventarioId], [StockEnUbicacion], [StockDelSistema], [StockDiferencia], [LibroEnUbicacionId], [InventarioId]) VALUES (14, 18, 20, -2, 4, 11)
GO
SET IDENTITY_INSERT [dbo].[LineaInventarios] OFF
GO
SET IDENTITY_INSERT [dbo].[Ubicacions] ON 
GO
INSERT [dbo].[Ubicacions] ([UbicacionId], [Cantidad_Estantes], [Libros_Maximos], [Ocupada]) VALUES (1, 22, 4, 1)
GO
INSERT [dbo].[Ubicacions] ([UbicacionId], [Cantidad_Estantes], [Libros_Maximos], [Ocupada]) VALUES (2, 12, 9, 1)
GO
INSERT [dbo].[Ubicacions] ([UbicacionId], [Cantidad_Estantes], [Libros_Maximos], [Ocupada]) VALUES (3, 1000, 100, 1)
GO
SET IDENTITY_INSERT [dbo].[Ubicacions] OFF
GO
/****** Object:  Index [IX_LibroEnUbicacions_LibrosLibroId]    Script Date: 26/10/2020 12:15:25 ******/
CREATE NONCLUSTERED INDEX [IX_LibroEnUbicacions_LibrosLibroId] ON [dbo].[LibroEnUbicacions]
(
	[LibrosLibroId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LibroEnUbicacions_UbicacionId]    Script Date: 26/10/2020 12:15:25 ******/
CREATE NONCLUSTERED INDEX [IX_LibroEnUbicacions_UbicacionId] ON [dbo].[LibroEnUbicacions]
(
	[UbicacionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LineaInventarios_InventarioId]    Script Date: 26/10/2020 12:15:25 ******/
CREATE NONCLUSTERED INDEX [IX_LineaInventarios_InventarioId] ON [dbo].[LineaInventarios]
(
	[InventarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LineaInventarios_LibroEnUbicacionId]    Script Date: 26/10/2020 12:15:25 ******/
CREATE NONCLUSTERED INDEX [IX_LineaInventarios_LibroEnUbicacionId] ON [dbo].[LineaInventarios]
(
	[LibroEnUbicacionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LibroEnUbicacions]  WITH CHECK ADD  CONSTRAINT [FK_LibroEnUbicacions_Libros_LibrosLibroId] FOREIGN KEY([LibrosLibroId])
REFERENCES [dbo].[Libros] ([LibroId])
GO
ALTER TABLE [dbo].[LibroEnUbicacions] CHECK CONSTRAINT [FK_LibroEnUbicacions_Libros_LibrosLibroId]
GO
ALTER TABLE [dbo].[LibroEnUbicacions]  WITH CHECK ADD  CONSTRAINT [FK_LibroEnUbicacions_Ubicacions_UbicacionId] FOREIGN KEY([UbicacionId])
REFERENCES [dbo].[Ubicacions] ([UbicacionId])
GO
ALTER TABLE [dbo].[LibroEnUbicacions] CHECK CONSTRAINT [FK_LibroEnUbicacions_Ubicacions_UbicacionId]
GO
ALTER TABLE [dbo].[LineaInventarios]  WITH CHECK ADD  CONSTRAINT [FK_LineaInventarios_Inventarios_InventarioId] FOREIGN KEY([InventarioId])
REFERENCES [dbo].[Inventarios] ([InventarioId])
GO
ALTER TABLE [dbo].[LineaInventarios] CHECK CONSTRAINT [FK_LineaInventarios_Inventarios_InventarioId]
GO
ALTER TABLE [dbo].[LineaInventarios]  WITH CHECK ADD  CONSTRAINT [FK_LineaInventarios_LibroEnUbicacions_LibroEnUbicacionId] FOREIGN KEY([LibroEnUbicacionId])
REFERENCES [dbo].[LibroEnUbicacions] ([LibroEnUbicacionId])
GO
ALTER TABLE [dbo].[LineaInventarios] CHECK CONSTRAINT [FK_LineaInventarios_LibroEnUbicacions_LibroEnUbicacionId]
GO
