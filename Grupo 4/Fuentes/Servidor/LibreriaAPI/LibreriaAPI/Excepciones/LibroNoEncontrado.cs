﻿using System;


namespace LibreriaAPI.Excepciones
{
    public class LibroNoEncontrado : Exception
    {
        public LibroNoEncontrado(string pCadena) : base(pCadena)
        {

        }
    }
}
