﻿using System;

namespace LibreriaAPI.Excepciones
{
    public class InventarioNoEncontrado : Exception
    {
        public InventarioNoEncontrado(string pCadena) : base(pCadena)
        {

        }
    }
}