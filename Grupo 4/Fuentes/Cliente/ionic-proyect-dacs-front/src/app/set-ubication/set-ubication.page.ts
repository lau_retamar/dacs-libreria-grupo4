import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { analytics } from 'firebase';
import { AlertService } from '../services/alert-service/alert.service';
import { Book, BookService } from '../services/book-service/book.service';
import { LoadService } from '../services/load-service/load.service';

@Component({
  selector: 'app-set-ubication',
  templateUrl: './set-ubication.page.html',
  styleUrls: ['./set-ubication.page.scss'],
})
export class SetUbicationPage implements OnInit {
  public isbnToSearch: number;
  public book: Book;
  public newUbication: number;
  
  constructor(private bookService: BookService,
              public loadService: LoadService,
              private alertService: AlertService,
              private router: ActivatedRoute,
              private barcode: BarcodeScanner) { }

  ngOnInit() {
    let isbn = this.router.snapshot.paramMap.get('param');
    if (isbn != null) {
      this.loadService.presentLoading("Obteniendo libro...").then(presentLoading => {
        this.bookService.getBookByISBN(isbn).then((data: any) => {
          this.loadService.loadingController.dismiss();
          this.book = data;
        }).catch(
          error => {
            this.loadService.loadingController.dismiss();
            this.alertService.presentAlert("Error", "Error al obtener libro", error.error);
        });
      })
    }
  }

  scan(){
    this.barcode.scan().then((barcodeData) => {
      alert(barcodeData.text);
    }, (err) => {
      alert(JSON.stringify(err));
    })
  }

  searchByISBN(pISBN){
    this.loadService.presentLoading("Buscando libro...").then(presentLoading => {
      this.book = null;
      this.bookService.getBookByISBN(pISBN).then((data: any) => {
      this.loadService.loadingController.dismiss();
      this.book = data;
    }).catch(
      error => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert("Error", "No se encontro ISBN ingresado",error.error);
      }
    )
    })
  }

    updateUbication(pISBN, pUbication){
      this.loadService.presentLoading("Actualizando ubiación...").then(presentLoading => {
        this.bookService.updateUbication(pISBN, pUbication).then((data:any) => {
          this.loadService.loadingController.dismiss();
          this.alertService.presentAlert("Aviso", "Ubiación actualizada con éxito", "");
          this.book.ubicacionId = this.newUbication;
          this.newUbication = null;
        })
        .catch(
          error => {
            this.loadService.loadingController.dismiss();
            this.alertService.presentAlert("Error", "No se pudo actualizar ubicación", "");
          } 
        )
      })
  }

}
