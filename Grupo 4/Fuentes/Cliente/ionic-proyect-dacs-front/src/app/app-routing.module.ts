import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'search-book',
    loadChildren: () => import('./search-book/search-book.module').then( m => m.SearchBookPageModule)
  },
  {
    path: 'add-book',
    loadChildren: () => import('./add-book/add-book.module').then( m => m.AddBookPageModule)
  },
  {
    path: 'book-list',
    loadChildren: () => import('./book-list/book-list.module').then( m => m.BookListPageModule)
  },
  {
    path: 'update-stock/:param',
    loadChildren: () => import('./update-stock/update-stock.module').then( m => m.UpdateStockPageModule)
  },
  {
    path: 'update-stock',
    loadChildren: () => import('./update-stock/update-stock.module').then( m => m.UpdateStockPageModule)
  },
  {
    path: 'set-ubication/:param',
    loadChildren: () => import('./set-ubication/set-ubication.module').then( m => m.SetUbicationPageModule)
  },
  {
    path: 'set-ubication',
    loadChildren: () => import('./set-ubication/set-ubication.module').then( m => m.SetUbicationPageModule)
  },
  {
    path: 'modify-book',
    loadChildren: () => import('./modify-book/modify-book.module').then( m => m.ModifyBookPageModule)
  },
  {
    path: 'delete-book',
    loadChildren: () => import('./delete-book/delete-book.module').then( m => m.DeleteBookPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'inventory-control',
    loadChildren: () => import('./inventory-control/inventory-control.module').then( m => m.InventoryControlPageModule)
  }





];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
