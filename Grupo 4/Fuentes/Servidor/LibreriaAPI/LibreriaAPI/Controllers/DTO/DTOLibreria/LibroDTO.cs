﻿using System;


namespace LibreriaAPI.Controllers.DTO
{
    public class LibroDTO
    {
        public string ISBN { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public DateTime fechaPublicacion { get; set; }
        public string editorial { get; set; }
        public string idioma { get; set; }
        public int stock { get; set; }
        public int ubicacionId { get; set; }


    }
}
