﻿using System.Collections.Generic;


namespace LibreriaAPI.Controllers.DTO
{
    public class ApiGoogleLibroDTO
    {
        public string totalItems { get; set; }
        public IList<ItemsDTO> items { get; set; }
    }
}
