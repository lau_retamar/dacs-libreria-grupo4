import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModifyBookPageRoutingModule } from './modify-book-routing.module';

import { ModifyBookPage } from './modify-book.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModifyBookPageRoutingModule
  ],
  declarations: [ModifyBookPage]
})
export class ModifyBookPageModule {}
