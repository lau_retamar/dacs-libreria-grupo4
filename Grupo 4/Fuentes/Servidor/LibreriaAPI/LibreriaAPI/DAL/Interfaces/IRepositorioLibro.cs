﻿using LibreriaAPI.Modelo;

namespace LibreriaAPI.DAL.Interfaces
{
   
    public interface IRepositorioLibro : IRepositorioGenerico<Libro>
    {
    }
}
