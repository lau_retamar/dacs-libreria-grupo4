import { Component, OnInit } from '@angular/core';
import { AlertService } from '../services/alert-service/alert.service';
import { Book, BookService } from '../services/book-service/book.service';
import { LoadService } from '../services/load-service/load.service';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.page.html',
  styleUrls: ['./book-list.page.scss'],
})
export class BookListPage implements OnInit {
  public books: [Book];

  constructor(private bookService: BookService,
    public loadService: LoadService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.getAllBooks();
  }

  getAllBooks() {
    this.loadService.presentLoading("Obteniendo Libros...").then(presentLoading => {
      this.bookService.getAllBooks().then((data: any) => {
        this.loadService.loadingController.dismiss();
        this.books = data;
      }).catch(
        error => {
          this.loadService.loadingController.dismiss();
          this.alertService.presentAlert("Error", "No se obtuvieron los libros", error.error);
        }
      )
    })
  }

  deleteBook(pISBN) {
    this.alertService.presentConfirmation('Alerta', '¿Desea eliminar el libro?', 'Cancelar', 'Aceptar').then((res: any) => {
      if (res == 'ok') {
        this.loadService.presentLoading("Eliminando libro..").then(presentLoading => {
          this.bookService.deleteBook(pISBN).then((res) => {

            //eliminación de elemento de la lista una vez eliminado.
            for (let index = 0; index < this.books.length; index++) {
              if (this.books[index].isbn == pISBN) {
                this.books.splice(index, 1);
                break;
              }
            }

            this.loadService.loadingController.dismiss();
            this.alertService.presentAlert('Aviso', 'Libro eliminado', 'El libro se eleminó correctamente');
          }).catch(
            error => {
              this.loadService.loadingController.dismiss();
              this.alertService.presentAlert("Error", "Error al eliminar libro", error.error);
            })
        })
      }
    })
  }

}
