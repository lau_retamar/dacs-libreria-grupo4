﻿using LibreriaAPI.Modelo;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL
{
    public static class DbInicializar
    {
        public static void Inicializar(IServiceProvider serviceProvider)
        {
            var iUdt = new UnidadDeTrabajo(serviceProvider.GetRequiredService<DbLibreriaContext>());
           
            Task<LibroEnUbicacion> iLibroUbicacion = iUdt.RepositorioLibroEnUbicacion.Obtener(1);
                if (iLibroUbicacion.Result != null)
                {
                    return;
                }
            #region Cargo primer libro y ubicacion
            Ubicacion mUbicacion = new Ubicacion
            {
                Cantidad_Estantes = 10,
                Libros_Maximos = 8,
                Ocupada = true,
            };

            Libro mLibro = new Libro()
            {
                ISBN = "1338218395",
                Autor = "Rowling J.K",
                Editorial = "Paperback",
                Baja = false,
                Fecha_Alta = DateTime.Now,
                Fecha_Publicacion = Convert.ToDateTime("20/08/2020"),
                Idioma = "Ingles",
                Titulo = "Harry Potter 7 Special Edition",
            };
            LibroEnUbicacion iLibroEnUbicacion = new LibroEnUbicacion()
            {
                Stock = 10,
                Libros = mLibro,
                Ubicacion = mUbicacion

            };
            iUdt.RepositorioUbicacion.Agregar(mUbicacion);
            iUdt.RepositorioLibro.Agregar(mLibro);
            iUdt.RepositorioLibroEnUbicacion.Agregar(iLibroEnUbicacion);
            iUdt.Guardar().Wait();
            #endregion
            #region Cargo segundo libro y ubicacion
            Ubicacion mUbicacion2 = new Ubicacion
            {
                Cantidad_Estantes = 5,
                Libros_Maximos = 5,
                Ocupada = true,
            };

            Libro mLibro2 = new Libro()
            {
                ISBN = "424",
                Autor = "Miguel de Cervantes",
                Editorial = "Hoby Club",
                Baja = false,
                Fecha_Alta = DateTime.Now,
                Fecha_Publicacion = Convert.ToDateTime("20/08/2005"),
                Idioma = "Español",
                Titulo = "Don Quijote",
            };
            LibroEnUbicacion iLibroEnUbicacion2 = new LibroEnUbicacion()
            {
                Stock = 15,
                Libros = mLibro2,
                Ubicacion = mUbicacion2

            };
            iUdt.RepositorioUbicacion.Agregar(mUbicacion2);
            iUdt.RepositorioLibro.Agregar(mLibro2);
            iUdt.RepositorioLibroEnUbicacion.Agregar(iLibroEnUbicacion2);
           iUdt.Guardar().Wait();
            #endregion
            #region Cargo tercer libro y ubicacion
            Ubicacion mUbicacion3 = new Ubicacion
            {
                Cantidad_Estantes = 7,
                Libros_Maximos = 9,
                Ocupada = true,
            };

            Libro mLibro3 = new Libro()
            {
                ISBN = "986304",
                Autor = "Martinez Mendez",
                Editorial = "Murcia:Kiosko",
                Baja = false,
                Fecha_Alta = DateTime.Now,
                Fecha_Publicacion = Convert.ToDateTime("10/06/2004"),
                Idioma = "Español",
                Titulo = "Recuperación de información",
            };
            LibroEnUbicacion iLibroEnUbicacion3 = new LibroEnUbicacion()
            {
                Stock = 25,
                Libros = mLibro3,
                Ubicacion = mUbicacion3

            };
            iUdt.RepositorioUbicacion.Agregar(mUbicacion3);
            iUdt.RepositorioLibro.Agregar(mLibro3);
            iUdt.RepositorioLibroEnUbicacion.Agregar(iLibroEnUbicacion3);
            #endregion


            iUdt.Guardar().Wait();
            }
        }
    }

