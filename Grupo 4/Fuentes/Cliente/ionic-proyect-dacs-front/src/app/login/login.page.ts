import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { MenuController} from '@ionic/angular';
import { AuthService } from '../services/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public validations_form: FormGroup;
  public errorMessage: string = '';
  public loading: boolean = false;

  validation_messages = {
   'email': [
     { type: 'required', message: 'El correo electrónico es requerido.' },
     { type: 'pattern', message: 'Ingrese un correo electrónico válido.' }
   ],
   'password': [
     { type: 'required', message: 'La contraseña es requerida.' },
     { type: 'minlength', message: 'La contraseña debe tener al menos 5 caracteres.' }
   ]
 };

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router,
              private menu: MenuController) {
    
    this.menu.enable(false);
  }

  ngOnInit() {
    
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }

  tryLogin(value){
    this.loading = true;
    this.authService.doLogin(value)
    .then(res => {
      this.router.navigate(["/search-book"]);
    }, err => {
      if (err.code == "auth/user-not-found" || err.code == "auth/wrong-password" ) {
        this.errorMessage = "Contraseña o correo electrónico incorrecto";
      } else {
        this.errorMessage = err.message;
      }
    }).finally(
      () => {
        this.loading = false;
      }
    );
  }

  ionViewWillEnter(){
    this.validations_form.controls.email.setValue('');
    this.validations_form.controls.email.markAsPristine();
    this.validations_form.controls.email.markAsUntouched();

    this.validations_form.controls.password.setValue('');
    this.validations_form.controls.password.markAsPristine();
    this.validations_form.controls.password.markAsUntouched();
  }

  /* activateMenu(){
    this.menu.enable(true);
    this.navCtrl.navigateRoot("/search-book");
  } */ 

}

