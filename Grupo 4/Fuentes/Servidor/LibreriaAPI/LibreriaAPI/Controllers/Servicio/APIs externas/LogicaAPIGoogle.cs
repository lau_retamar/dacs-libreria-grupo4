﻿using AutoMapper;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.DAL;
using LibreriaAPI.Excepciones;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LibreriaAPI.Controllers.Servicio.APIs_externas
{
    public class LogicaAPIGoogle
    {
        private readonly IUnidadDeTrabajo iUdt;
        private readonly IMapper iMapper;

        public LogicaAPIGoogle(IUnidadDeTrabajo pUdtcontext, IMapper pMapper)
        {
            iUdt = pUdtcontext;
            iMapper = pMapper;
        }
        public async Task<ActionResult<LibroDTO>> ObtenerLibroGooglePorISBN(string pISBN)
        {
            string idioma = "";
            var googlestring = await APIGoogleBooks.ConsultarISBNAsync(pISBN);
            ApiGoogleLibroDTO mApiGoogleDTO = JsonConvert.DeserializeObject<ApiGoogleLibroDTO>(googlestring);
            if (mApiGoogleDTO.totalItems != "0")
            {
                if (mApiGoogleDTO.items[0].volumeInfo.language == "es")
                {
                    idioma = "Español";
                }
                else
                {
                    idioma = "Ingles";
                }
                LibroDTO mLibro = new LibroDTO()
                {
                    ISBN = pISBN,
                    titulo = mApiGoogleDTO.items[0].volumeInfo.title,
                    autor = Convert.ToString(mApiGoogleDTO.items[0].volumeInfo.authors.ElementAt(0)),
                    fechaPublicacion = Convert.ToDateTime(mApiGoogleDTO.items[0].volumeInfo.publishedDate),
                    editorial = mApiGoogleDTO.items[0].volumeInfo.publisher,
                    idioma = idioma,
                };
                return mLibro;
            }
            else
            {
                throw new LibroEnApiGoogleNoEncontrado("Google no encontro libro");
            }
        
        }
    }
}


