﻿using System;


namespace LibreriaAPI.Excepciones
{
    public class UbicacionExistente:Exception
    {
        public UbicacionExistente(string pCadena):base(pCadena)
        {

        }
    }
}
