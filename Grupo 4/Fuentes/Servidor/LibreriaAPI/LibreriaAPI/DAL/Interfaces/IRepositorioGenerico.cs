﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL.Interfaces
{
    /// <summary>
    /// Interfaz generica del Repositorio
    /// </summary>
    /// <typeparam name="TEntity">Una entidad del Dominio</typeparam>
    public interface IRepositorioGenerico<TEntity> where TEntity : class
    {
        Task Agregar(TEntity pEntity);
        void Eliminar(TEntity pEntity);
        Task<IEnumerable<TEntity>> ObtenerTodos();
        Task<TEntity> Obtener(int pId);
        void ModificarEntidades(TEntity pEntity);
    }
}
