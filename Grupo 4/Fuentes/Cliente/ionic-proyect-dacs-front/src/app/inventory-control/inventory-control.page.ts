import { convertActionBinding } from '@angular/compiler/src/compiler_util/expression_converter';
import { TypeModifier } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { AlertService } from '../services/alert-service/alert.service';
import { Book } from '../services/book-service/book.service';
import { InventoryService } from '../services/inventory-service/inventory.service';
import { LoadService } from '../services/load-service/load.service';

@Component({
  selector: 'app-inventory-control',
  templateUrl: './inventory-control.page.html',
  styleUrls: ['./inventory-control.page.scss'],
})
export class InventoryControlPage implements OnInit {
  public books: any;
  public ubicationToSearch: number;

  constructor(private inventoryService: InventoryService,
              private loadService: LoadService,
              private alertService: AlertService) { }

  ngOnInit() {}

  getBooksOfUbication(pUbication: number){
    this.loadService.presentLoading("Obteniendo Libros...").then(presentLoading => {
      this.inventoryService.getBooksOfObication(pUbication).then((data: any) => {
        this.loadService.loadingController.dismiss();
        this.books = data;
      }).catch(error => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert("Error", "No se obtuvieron los libros", error.error);
      })
    })
  }

  hide(pBook) {
    if (pBook.hideInformation == false) {
      pBook.hideInformation = true;
    }
    else{
      pBook.hideInformation = false;
    }
  }

  updateStockOfBook($event, pBook){
    pBook.stockEnUbicacion = Number($event);
    console.log(pBook.stockEnUbicacion);
  }

  addInventory(){
    console.log(this.books);
    this.loadService.presentLoading("Agregando inventario...").then(presentLoading => {
      this.inventoryService.addInventory(this.books).then((res) => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert('Éxito', 'Inventario registrado', 'El inventario se registró exitosamente');
      }).catch((error) => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert('Error', 'Error en al registrar inventario', error.error);
      });
    })
  }

}
