import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModifyBookPage } from './modify-book.page';

describe('ModifyBookPage', () => {
  let component: ModifyBookPage;
  let fixture: ComponentFixture<ModifyBookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyBookPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModifyBookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
