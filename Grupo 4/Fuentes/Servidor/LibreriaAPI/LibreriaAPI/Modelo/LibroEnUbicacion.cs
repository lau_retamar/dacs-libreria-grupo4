﻿using System.Collections.Generic;


namespace LibreriaAPI.Modelo
{
    public class LibroEnUbicacion
    {
     
        public int LibroEnUbicacionId { get; set; }
        public int  Stock { get; set; }
        public virtual Libro Libros { get; set; }
   
        public virtual Ubicacion Ubicacion { get; set; }
        public virtual IList<LineaInventario> LineaInventarios { get; set; }

    }
}
