﻿using System;


namespace LibreriaAPI.Excepciones
{
    public class InventarioExistente : Exception
    {
        public InventarioExistente(string pCadena) : base(pCadena)
        {

        }
    }
}
