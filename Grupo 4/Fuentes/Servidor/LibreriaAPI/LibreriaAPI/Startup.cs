using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using LibreriaAPI.Controllers.Servicio.APIs_externas;
using LibreriaAPI.Controllers.Servicio.LogicaDelNegocio;
using LibreriaAPI.Controllers.Servicios.Mapper;
using LibreriaAPI.DAL;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;


namespace LibreriaAPI
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DbLibreriaContext>(options => options.UseLazyLoadingProxies()
            .UseSqlServer(
                 Configuration.GetConnectionString("DbLibreriaContext"), sqlServerOptions => sqlServerOptions.MigrationsAssembly("LibreriaAPI")));

          
            services.AddSwaggerGen(c =>
            {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "Mi API LIBRERIA", Version = "V1" });
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            c.IncludeXmlComments(xmlPath);

            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description= "JWT Token usar Bearer {token}",
                    Name = "Authorization",
                    In=ParameterLocation.Header,
                    Type= SecuritySchemeType.ApiKey,
                    Scheme="Bearer"
            });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    { new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type=ReferenceType.SecurityScheme,
                            Id="Bearer"
                        },
                        Scheme="oauth2",
                        Name="Bearer",
                        In=ParameterLocation.Header,
                    },
                    new List<string>()
                    }

                });
            });
            services.AddScoped<LogicaUbicacion>();
            services.AddScoped<LogicaLibro>();
            services.AddScoped<LogicaInventario>();
            services.AddScoped<LogicaAPIGoogle>();
            services.AddScoped<IUnidadDeTrabajo, UnidadDeTrabajo>((serviceProvider) =>
            {
                var dbContext = serviceProvider.GetService<DbLibreriaContext>();

                return new UnidadDeTrabajo(dbContext);
            });

            var builderJsonFirebase = new ConfigurationBuilder()
               .AddJsonFile("ConfiguracionFireBase.json", optional: false, reloadOnChange: true);
            var configuracionFirebase= builderJsonFirebase.Build();
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(optiones =>
                {
                    optiones.Authority = configuracionFirebase["FireBase:Authority"];
                    optiones.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = configuracionFirebase["FireBase:ValidIssuer"],
                        ValidateAudience = true,
                        ValidAudience = configuracionFirebase["FireBase:ValidAudience"],
                        ValidateLifetime = true
                    };
                });

              
            services.AddAutoMapper(typeof(DominioAVista));

           
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()

                      ;
                    });
            });
            services.AddControllers();

        }

        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.Use(async (context, next) =>
            {
                var metodo = context.Request.Headers["access-control-request-method"].ToString();
                var path = context.Request.Path.ToString();
                var statusCodeRespuesta = context.Response.StatusCode.ToString();
                var ipOrigen = context.Request.Headers["origin"].ToString();
                Log.log.Info("Tipo operacion: " + metodo + " Ur: " + path + " Ip Origen: " + ipOrigen + " StatusCode: " + statusCodeRespuesta);
                await next.Invoke();
            });

            app.UseExceptionHandler(
             options => {
                 options.Run(
                   async context =>
                   {
                       context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                       context.Response.ContentType = "text/plain";
                       var exepcion = context.Features.Get<IExceptionHandlerFeature>();
                       var nameExcepcion = exepcion.Error.GetType().Name switch
                       {
                           "InventarioExistente" => HttpStatusCode.PreconditionFailed,
                           "InventarioNoEncontrado" => HttpStatusCode.NotFound,
                           "LibroEnApiGoogleNoEncontrado" => HttpStatusCode.NotFound,
                           "LibroExistente" => HttpStatusCode.PreconditionFailed,
                           "LibroNoEncontrado" => HttpStatusCode.NotFound,
                           "StockNoValido" => HttpStatusCode.PreconditionFailed,
                           "UbicacionExistente" => HttpStatusCode.PreconditionFailed,
                           "UbicacionNoEncontrada" => HttpStatusCode.NotFound,
                           _ => HttpStatusCode.ServiceUnavailable
                       };
                       
                       var mensaje = context.Features.Get<IExceptionHandlerFeature>().Error.GetBaseException().Message;
                       var metodo = context.Request.Headers["access-control-request-method"].ToString();
                       var path = context.Request.Path.ToString();
                       var statusCodeRespuesta = context.Response.StatusCode.ToString();
                       var ipOrigen = context.Request.Headers["origin"].ToString();
                       Log.log.Error("Error: " + mensaje + " Tipo operacion: " + metodo + " Ur: " + path + " Ip Origen: " + ipOrigen + " StatusCode: " + statusCodeRespuesta);

                       context.Response.StatusCode = (int)nameExcepcion;
                       var cont = Encoding.UTF8.GetBytes($"{exepcion.Error.Message}");
                       _ = context.Response.Body.WriteAsync(cont, 0, cont.Length);
                       await Task.CompletedTask;
                   });
             });


            app.UseHttpsRedirection();
            app.UseRouting();
           
            app.UseSwagger();
           
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            
            app.UseCors();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
