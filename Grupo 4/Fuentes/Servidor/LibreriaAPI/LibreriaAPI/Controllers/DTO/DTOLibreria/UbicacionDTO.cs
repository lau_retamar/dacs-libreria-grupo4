﻿namespace LibreriaAPI.Controllers.DTO
{
    public class UbicacionDTO
    {
        public int id { get; set; }
        public int cantidadEstantes { get; set; }
        public int librosMaximos { get; set; }
        public bool ocupado { get; set; }

    }
}
