﻿using LibreriaAPI.DAL.Interfaces;
using LibreriaAPI.Modelo;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL.Repositorio
{
   public  class RepositorioInventario : RepositorioGenerico<Inventario, DbLibreriaContext>, IRepositorioInventario
    {
      
        public RepositorioInventario(DbLibreriaContext pContext) : base(pContext)
        {

        }
        public bool ConsultarExistencia(int pid)
        {
           return iDbContext.Inventarios.Any(x => x.InventarioId == pid);
        }
        public async Task<IList<LibroEnUbicacion>> ObtengoListaTodosLosLibrosPorUbicacion(int pIdUbicacion)
        {
            var libroUbicacion= (from libroenUbicacion in iDbContext.LibroEnUbicacions
                                 where ((libroenUbicacion.Ubicacion.UbicacionId==pIdUbicacion) )
                                 select libroenUbicacion).ToListAsync();
            return await libroUbicacion;
        }

    }
}
