﻿using LibreriaAPI.DAL.Interfaces;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL
{
   public interface IUnidadDeTrabajo
    {
        IRepositorioInventario RepositorioInventario { get; }
        IRepositorioLibro RepositorioLibro { get; }
        IRepositorioLibroEnUbicacion RepositorioLibroEnUbicacion { get; }
        IRepositorioLineaInventario RepositorioLineaInventario { get; }
        IRepositorioUbicacion RepositorioUbicacion { get; }
        Task Guardar();
    }
}
