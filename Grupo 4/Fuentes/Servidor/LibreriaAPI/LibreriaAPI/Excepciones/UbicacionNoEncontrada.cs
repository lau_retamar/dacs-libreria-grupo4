﻿using System;

namespace LibreriaAPI.Excepciones
{
    public class UbicacionNoEncontrada : Exception
    {
        public UbicacionNoEncontrada(string pCadena) : base(pCadena)
        {

        }
    }
}
