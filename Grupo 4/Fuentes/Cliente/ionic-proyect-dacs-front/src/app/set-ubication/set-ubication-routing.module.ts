import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetUbicationPage } from './set-ubication.page';

const routes: Routes = [
  {
    path: '',
    component: SetUbicationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetUbicationPageRoutingModule {}
