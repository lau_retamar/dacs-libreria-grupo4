﻿using System;


namespace LibreriaAPI.Excepciones
{
    public class StockNoValido : Exception
    {
        public StockNoValido(string pCadena) : base(pCadena)
        {

        }
    }
}
