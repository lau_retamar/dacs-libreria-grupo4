﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.Controllers.Servicio.LogicaDelNegocio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;



namespace LibreriaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LibroController : ControllerBase
    {
        private LogicaLibro iLogicaLibro;
        public LibroController(LogicaLibro pLogicaLibro)
        {
            iLogicaLibro = pLogicaLibro;
        }
        /// <summary>
        /// Obtengo listado de todos los libros 
        /// </summary>
        /// <returns>Devuelve un listado de todos los libros</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LibroDTO>>> GetLibroDTO()
        {
                return await iLogicaLibro.ObtenerLibrosEnUbicacionDTO();
        }
        /// <summary>
        /// Obtiene libro
        /// </summary>
        /// <param name="pISBN">busca por isbn</param>
        /// <returns>Devuelve libro en ubicacion buscado por isbn </returns>
        [HttpGet("{pISBN}")]
        public async Task<ActionResult<LibroDTO>> GetLibroDTO(string pISBN)
        {
                return await iLogicaLibro.ObtenerLibroEnUbicacionPorISBNDTO(pISBN);
        }
        /// <summary>
        /// Elimina un libro
        /// </summary>
        /// <param name="pISBN">busca por isbn</param>
        /// <returns>libro eliminado</returns>
        [HttpDelete("{pISBN}")]
        public async Task<ActionResult> DeleteUbicacionDTO(string pISBN)
        {
                await iLogicaLibro.EliminarLibroDTO(pISBN);
                return Ok("Se elimino libro");
        }
       /// <summary>
       /// Agrega un libro
       /// </summary>
       /// <param name="pLibroDTO"></param>
       /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> PostLibroDTO(LibroDTO pLibroDTO)
        {
                await iLogicaLibro.AgregarLibro(pLibroDTO);
                return Ok("Se registró Libro");
        }
        /// <summary>
        /// Modificar libro
        /// </summary>
        /// <param name="pLibroDTO"></param>
        /// <returns>Libro Modificado</returns>
        [HttpPut]
        public async Task<IActionResult> PutLibroDTO(LibroDTO pLibroDTO)
        {
                await iLogicaLibro.ModificarLibro(pLibroDTO);
                return Ok("Se modificó Libro");
        }
        /// <summary>
        /// Obtengo listado de todos los libros  sin ubicacion
        /// </summary>
        /// <returns>Devuelve un listado de todos los libros sin ubicacion</returns>
        [HttpGet("GetListadoLibrosSinUbicacion")]
        public async Task<ActionResult<IEnumerable<LibroDTO>>> GetListadoLibrosSinUbicacion()
        {
                return await iLogicaLibro.ObtenerLibrosSinUbicacionDTO();
        }
        /// <summary>
        /// Asignar  ubicacion a libro
        /// </summary>
        /// <param name="pLibroDTO"></param>
        /// <returns>Libro con ubicacion asignada</returns>
        [HttpPut("PutAsignarUbicacion")]
        public async Task<IActionResult> PutAsignarUbicacion(LibroDTO pLibroDTO)
        {
                await iLogicaLibro.ModificarLibro(pLibroDTO);
                return Ok("Se modificó Libro");
        }
        /// <summary>
        /// Obtengo listado stock de los libros 
        /// </summary>
        /// <returns>Devuelve un listado stock de los libros </returns>
        [HttpGet("GetListadoStock")]
        public async Task<ActionResult<IEnumerable<LibroDTO>>> GetListadoStock()
        {
                return await iLogicaLibro.ObtenerLibrosEnUbicacionDTO();
        }
        /// <summary>
        /// Actualiza el stock de un libro
        /// </summary>
        /// <param name="pISBN"></param>
        /// <param name="pStock"></param>
        /// <returns>Stock Atualizado</returns>
        [HttpPut("ActualizarStock")]
        public async Task<IActionResult> ActualizarStock(string pISBN,int pStock)
        {
                await iLogicaLibro.ModificarLibroStock(pISBN, pStock);
                return Ok("Se Actualizo Stock Libro");
        }
        /// <summary>
        /// Actualizo ubicacion de un libro
        /// </summary>
        /// <param name="pISBN"></param>
        /// <param name="pUbicacionId"></param>
        /// <returns>Ubicación de libro actualizada</returns>
        [HttpPut("ActualizarUbicaciónDeLibro")]
        public async Task<IActionResult> ActualizarUbicaciónDeLibro(string pISBN, int pUbicacionId)
        {
                await iLogicaLibro.ModificarLibroUbicacion(pISBN, pUbicacionId);
                return Ok("Se Actualizo Ubicación del Libro");
        }
    }
}
