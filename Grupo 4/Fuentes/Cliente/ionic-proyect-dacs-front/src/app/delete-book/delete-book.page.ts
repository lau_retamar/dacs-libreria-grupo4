import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { resolve } from 'dns';
import { AlertService } from '../services/alert-service/alert.service';
import { Book, BookService } from '../services/book-service/book.service';
import { LoadService } from '../services/load-service/load.service';

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.page.html',
  styleUrls: ['./delete-book.page.scss'],
})
export class DeleteBookPage implements OnInit {
  public book: Book;
  public isbnToSearch: number;

  constructor(private alertService: AlertService,
              private barcode: BarcodeScanner,
              private bookService: BookService,
              private loadService: LoadService) { }

  ngOnInit() {
  }
  
  scan(){
    this.barcode.scan().then((barcodeData) => {
      this.searchByISBN(barcodeData.text);
    }, (err) => {
      alert(JSON.stringify(err));
    })
  }

  deleteBook(pISBN){
    this.alertService.presentConfirmation('Alerta','¿Desea eliminar el libro?','Cancelar', 'Aceptar').then((res: any) => {
      if (res == 'ok') {
        this.loadService.presentLoading("Eliminando libro...").then(presentAlert => {
          this.bookService.deleteBook(pISBN).then((data: any) => {
            this.loadService.loadingController.dismiss();
            this.book = null;
            this.isbnToSearch = null;
            this.alertService.presentAlert("Éxito", "Libro eliminado", "Se eliminó el libro correctamente");
          }).catch(error => {
            this.loadService.loadingController.dismiss();
            this.alertService.presentAlert("Error", "No se pudo eliminar libro", error.error);
          })
        })
      }
    })
  }

  searchByISBN(pISBN){
    this.loadService.presentLoading("Buscando libro...").then(presentLoading => {
      this.bookService.getBookByISBN(pISBN).then((data: any) => {
        this.loadService.loadingController.dismiss();
        this.book = data;
      }).catch(error =>{
          this.loadService.loadingController.dismiss();
          this.alertService.presentAlert("Error", "No se encontro ISBN ingresado",error.error);
        })
      }
    )
  }
  
  cancel(){
    this.book = null;
    this.isbnToSearch = null;
  }

}
