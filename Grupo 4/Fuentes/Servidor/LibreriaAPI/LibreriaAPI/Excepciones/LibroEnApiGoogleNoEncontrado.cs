﻿using System;


namespace LibreriaAPI.Excepciones
{
    public class LibroEnApiGoogleNoEncontrado : Exception
    {
        public LibroEnApiGoogleNoEncontrado(string pCadena) : base(pCadena)
        {

        }
    }
}