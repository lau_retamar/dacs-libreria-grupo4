import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { MenuController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { AlertService } from '../services/alert-service/alert.service';
import { Book, BookService } from '../services/book-service/book.service';
import { LoadService } from '../services/load-service/load.service';

@Component({
  selector: 'app-modify-book',
  templateUrl: './modify-book.page.html',
  styleUrls: ['./modify-book.page.scss'],
})
export class ModifyBookPage implements OnInit {

  private isbnToSearch: number;
  private book: Book;

  private mTitulo: string;
  private mAutor: string;
  private mFecha; //No se que tipo va en las fechas
  private mFechaModificada; //No se que tipo va en las fechas
  
  private mEditorial: string;
  private mIdioma: string;

  constructor(
    private barcode: BarcodeScanner,
    private bookService: BookService,
    private alertService: AlertService,
    public loadService: LoadService,
    private menuCtrl: MenuController) { }

  ngOnInit() {
  }

  scan(){
    this.barcode.scan().then((barcodeData) => {
      this.searchByISBN(barcodeData.text);
    }, (err) => {
      alert(JSON.stringify(err));
    })
  }

  searchByISBN(pISBN) {
    this.loadService.presentLoading("Buscando libro...").then(presentLoading => {
      this.mTitulo = null;
      this.mAutor = null;
      this.mFecha = null;
      this.mFechaModificada = null;
      this.mEditorial = null;
      this. mIdioma = null;
      this.book = null;
  
      this.bookService.getBookByISBN(pISBN).then((data: any) => {
        this.book = data;
        this.loadService.loadingController.dismiss();
      }).catch(
        error => {
          this.loadService.loadingController.dismiss();
          this.alertService.presentAlert("Error", "No se encontro ISBN ingresado", error.error);
        }
      )
    })
  }

  modify() {
    this.loadService.presentLoading("Modificando libro...").then(presentLoading => {
      let pTitulo;
      if ((this.mTitulo == '' ) || (this.mTitulo == null)){
        pTitulo = this.book.titulo;
      }
      else{
        pTitulo = this.mTitulo;
      }

      let pAutor;
      if ((this.mAutor == '' ) || (this.mAutor == null)){
        pAutor = this.book.autor;
      }
      else{
        pAutor = this.mAutor;
      }

      let pFechaPublicacion;
      if ((this.mFechaModificada == '' ) || (this.mFechaModificada == null)){
        pFechaPublicacion = this.book.fechaPublicacion;
      }
      else{
        pFechaPublicacion = this.mFechaModificada;
      }

      let pEditorial;
      if ((this.mEditorial == '' ) || (this.mEditorial == null)){
        pEditorial = this.book.editorial;
      }
      else{
        pEditorial = this.mEditorial;
      }

      let pIdioma;
      if ((this.mIdioma == '' ) || (this.mIdioma == null)){
        pIdioma = this.book.idioma;
      }
      else{
        pIdioma = this.mIdioma;
      }

      let modifiedBook =
      {
        "isbn": this.book.isbn,
        "titulo": pTitulo,
        "autor": pAutor,
        "fechaPublicacion": pFechaPublicacion,
        "editorial": pEditorial,
        "idioma": pIdioma,
        "stock": this.book.stock,
        "ubicacionId": this.book.ubicacionId
      }

      this.bookService.updateBook(modifiedBook).then((data: any) => {
        this.loadService.loadingController.dismiss();
        this.alertService.presentAlert("Éxito", "Libro modificado", "Los datos del libro fueron modificados con éxito.");
      }).catch(
        error => {
          this.loadService.loadingController.dismiss();
          this.alertService.presentAlert("Error", "Error modificando", error.error);
        }
      )
    })
  }

  cancel(){
    this.book = null;
    this.isbnToSearch = null;
  }

}


