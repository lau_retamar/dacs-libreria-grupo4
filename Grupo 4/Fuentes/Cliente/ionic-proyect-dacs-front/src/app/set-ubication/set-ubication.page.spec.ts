import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetUbicationPage } from './set-ubication.page';

describe('SetUbicationPage', () => {
  let component: SetUbicationPage;
  let fixture: ComponentFixture<SetUbicationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetUbicationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetUbicationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
