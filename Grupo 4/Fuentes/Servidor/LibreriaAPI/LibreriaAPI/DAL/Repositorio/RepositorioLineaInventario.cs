﻿using LibreriaAPI.DAL.Interfaces;
using LibreriaAPI.Modelo;


namespace LibreriaAPI.DAL.Repositorio
{
    public class RepositorioLineaInventario : RepositorioGenerico<LineaInventario, DbLibreriaContext>, IRepositorioLineaInventario
    {  
        public RepositorioLineaInventario(DbLibreriaContext pContext) : base(pContext)
        {

        }
    }
}
