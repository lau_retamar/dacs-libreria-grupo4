﻿using LibreriaAPI.Modelo;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibreriaAPI.DAL.Interfaces
{
   
    public interface IRepositorioInventario : IRepositorioGenerico<Inventario>
    {
        Task<IList<LibroEnUbicacion>> ObtengoListaTodosLosLibrosPorUbicacion(int pIdUbicacion);
        bool ConsultarExistencia(int pid);
    }
}
