﻿using System;
using System.Collections.Generic;


namespace LibreriaAPI.Modelo
{
    public class Inventario
    {
        
        public int InventarioId { get; set; }
        public DateTime Fecha { get; set; }
        
       
        public virtual IList<LineaInventario> LineaInventarios { get; set; }


    }
}
