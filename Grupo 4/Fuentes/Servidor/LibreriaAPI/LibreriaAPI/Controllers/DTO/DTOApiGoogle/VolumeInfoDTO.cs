﻿namespace LibreriaAPI.Controllers.DTO
{
    public class VolumeInfoDTO
    {
        public string title { get; set; }
        public string[] authors { get; set; }
        public string description { get; set; }
        public string language { get; set; }
        public string publisher { get; set; }
        public string publishedDate { get; set; }
    }
}
