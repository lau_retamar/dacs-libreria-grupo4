import { Component, OnInit } from '@angular/core';

import { MenuController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './services/auth-service/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Buscar libro',
      url: 'search-book',
      icon: 'search'
    },
    {
      title: 'Agregar libro',
      url: 'add-book',
      icon: 'book'
    },
    {
      title: 'Listar libros',
      url: 'book-list',
      icon: 'list'
    },
    {
      title: 'Actualizar stock',
      url: 'update-stock',
      icon: 'bar-chart'
    },
    {
      title: 'Asignar ubicación',
      url: 'set-ubication',
      icon: 'location'
    },
    {
      title: 'Modificar libro',
      url: 'modify-book',
      icon: 'pencil'
    },
    {
      title: 'Eliminar libro',
      url: 'delete-book',
      icon: 'trash'
    },
    {
      title: 'Controlar inventario',
      url: 'inventory-control',
      icon: 'library'
    }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders']

  constructor(private platform: Platform,
              private splashScreen: SplashScreen,
              private statusBar: StatusBar,
              private router: Router,
              public afAuth: AngularFireAuth,
              private authService: AuthService,
              private menuCrtl: MenuController) {

    this.initializeApp();
  }

  initializeApp() {
    this.authService.doLogout();

    this.platform.ready().then(() => {
      this.afAuth.user.subscribe(user => {
        if(user){
          this.router.navigate(["/search-book"]);
        } else {
          this.router.navigate(["/login"]);
        }
      }, err => {
        this.router.navigate(["/login"]);
      }, () => {
        this.splashScreen.hide();
      })
      this.statusBar.styleDefault();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }

  logOut(){
    this.authService.doLogout()
    .then(res => {
      this.menuCrtl.enable(false);
      this.router.navigate(["/login"]);
    }, err => {
      console.log(err);
    })
  }

}
