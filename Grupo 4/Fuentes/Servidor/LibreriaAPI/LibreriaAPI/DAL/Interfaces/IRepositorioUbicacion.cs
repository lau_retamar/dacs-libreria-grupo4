﻿using LibreriaAPI.Modelo;

namespace LibreriaAPI.DAL.Interfaces
{
    
    public interface IRepositorioUbicacion : IRepositorioGenerico<Ubicacion>
    {
        bool ConsultarExistencia(int pid);
    }
    
}
