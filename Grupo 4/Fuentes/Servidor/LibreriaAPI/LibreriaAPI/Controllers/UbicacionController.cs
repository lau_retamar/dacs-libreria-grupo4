﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LibreriaAPI.Controllers.DTO;
using LibreriaAPI.Controllers.Servicio.LogicaDelNegocio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace LibreriaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UbicacionController : ControllerBase
    {
        private LogicaUbicacion iLogicaUbicacion;
        public UbicacionController(LogicaUbicacion pLogicaUbicacion)
        {
            iLogicaUbicacion = pLogicaUbicacion;
        }
        /// <summary>
        /// Listado de todas las ubicaciones
        /// </summary>
        /// <returns>Devuelve un listado de todas las ubicaciones</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UbicacionDTO>>> GetUbicacionDTO()
        {
                return await iLogicaUbicacion.ObtenerUbicacionDTO();
        }
        /// <summary>
        /// Obtiene una ubicacion
        /// </summary>
        /// <param name="id">busca por id de ubicacion</param>
        /// <returns>Devuelve unaUbicacion</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UbicacionDTO>> GetUbicacionDTO(int id)
        { 
                return await iLogicaUbicacion.ObtenerUbicacionPorIdDTO(id);
        }
        /// <summary>
        /// Modificar una ubicacion
        /// </summary>
        /// <param name="ubicacionDTO"></param>
        /// <returns>Ubicacion Modificada</returns>
        [HttpPut]
        public async Task<IActionResult> PutUbicacionDTO(UbicacionDTO ubicacionDTO)
        {
                await iLogicaUbicacion.ModificarUbicacion(ubicacionDTO);
                return Ok("Se modificó Ubicación");
        }
        /// <summary>
        /// Agregar una ubicacion
        /// </summary>
        /// <param name="ubicacionDTO"></param>
        /// <returns>Ubicacion registrada</returns>
        [HttpPost]
        public async Task<ActionResult> PostUbicacionDTO(UbicacionDTO ubicacionDTO)
        {
                await iLogicaUbicacion.AgregarUbicacion(ubicacionDTO);
                return Ok("Se registró Ubicación");
        }
        /// <summary>
        /// Elimina una ubicacion de la base de datos
        /// </summary>
        /// <param name="id">Id de la ubicacion a eliminar</param>
        /// <returns>Ubicacion eliminada</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUbicacionDTO(int id)
        {
                await iLogicaUbicacion.EliminarUbicacionDTO(id);
                return Ok("Se elimino ubicación");
        }
    }
}
